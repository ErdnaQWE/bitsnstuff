package fonts;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class Font {
		
	private Image[] characters = new Image[109384]; // Follow the UNICODE standard, shoud make things a little easier
	
	public void loadFont(Image[] characters) {
		this.characters = characters;
	}
	
	public void loadFont(String directory) {
		// Maybe change the parameter to a dictionnary or an array or something
		// This was static before, had some grand idea of loading being static, it was probably a good idea but now it's gone!
		// Load would have created the array from the images and would have returned it so it can be loaded into an instance
		
		// TODO List, why it is located here? I have no idea.
		// - Make sure I am able to make the sound engine work, somehow. Test with some random sounds, porbably the text appearance sounds.
		// - Complete the text thing, make sure I am able to pause and make all sorts of effects with the font object
	}
	
	public void drawString(Graphics g, int x, int y, String string) {
		
		int currentPosition = 0; // Current relative x position at which to draw the string.
		
		for (char ch: string.toCharArray()) {
			//g.drawImage(characters[ch], x + currentPosition, y - characters[ch].getHeight());
			if (ch == 103 || ch == 112) {
				characters[ch].drawFlash(x + currentPosition,  y - characters[ch].getHeight() + characters[ch].getHeight()/2 - 1, characters[ch].getWidth(), characters[ch].getHeight(), g.getColor());
			} else {
				characters[ch].drawFlash(x + currentPosition,  y - characters[ch].getHeight(), characters[ch].getWidth(), characters[ch].getHeight(),g.getColor());
			}
			currentPosition += characters[ch].getWidth() + 2;
		}
	}
	
	/* private void getImage(String string) {
		// Use this instead of drawString() once it is implemented
	} */
	
	public int getStringWidth(String string) {
		int total = 0;
		
		for (int i = 0; i < string.length(); i++) {
			total += characters[string.charAt(i)].getWidth() + 2;
		}
		
		return total;
	}
	
	// This should probably not be used because it will give weird (and often useless results)
	public int getStringHeight(String string) {
		int highest = 0;
		int current = Integer.MIN_VALUE;
		
		for (int i = 0; i < string.length(); i++) {
			current = characters[string.charAt(i)].getHeight();
			
			if (current > highest)
				highest = current;
		}
		
		return highest;
	}
	
	
}
