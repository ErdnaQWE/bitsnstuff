package util;

import org.newdawn.slick.Graphics;

import instances.Instance;


public class Box implements Solid {

	public float x;
	public float y;
	public float width;
	public float height;
	
	public Box() {
	}
	
	public Box(float x, float y, float width, float height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	public void render(Graphics g, float offsetx, float offsety) {
		g.drawRect(x + offsetx, y + offsety, width, height);
	}
	
	public void render(Graphics g) {
		render(g,0,0);
	}
	
	public float getEndX() {
		return (x + width);
	}
	
	public float getEndY() {
		return (y + height);
	}
	
	public float getHalfWidth() {
		return (width / 2);
	}
	
	public float getHalfHeight() {
		return (height / 2);
	}
	
	public float getCenterX() {
		return (x + getHalfWidth());
	}
	
	public float getCenterY() {
		return (y + getHalfHeight());
	}
	
	public boolean pixelHitTest(float relativeTargetX, float relativeTargetY) {
		return hitTest(new Box(relativeTargetX, relativeTargetY, 1, 1)); // I mean I guess?ddddddddadaddadasddd
	}
	
	public boolean hitTest(Box b) {
		return (b.getEndX() >= x && b.getEndY() >= y && getEndX() >= b.x && getEndY() >= b.y);
	}

	@Override
	public void collisionInstance(Instance element1, int delta) { //Bits & Stuff exclusive, remove in any other game.
		if (hitTest(element1)) { //Verifie pour une collision
			if (element1.v_speed > 0 && element1.y - (element1.v_speed * delta/1000.0f) < y - element1.height + 3) { //If and only if the element1 came from above the box. (The +3 allows the element1 to climb unleveled terrain, up to a difference of 3 pixels), the last condition is to prevent a glitch where if Bit stuck to a wall then jumped, he would be able to "wall jump"
				element1.y = y - element1.height;
				element1.v_speed = 0;
				element1.jumpReady = true;
			}
			else if (element1.h_speed > 0 && element1.y + element1.height > y) { // Approaches from the right
				//Verification pour les marches
				if (element1.y + element1.height - y <= 3) {
					//element1.x += 1; //Yes or no, maybe
					element1.y = y - element1.height;
				} else {
					element1.h_speed = 0;
					element1.x = x - element1.width - 1 ;
				}
			}
			else if (element1.h_speed < 0 && element1.y + element1.height > y) {
				//Verification pour les marches
				if (element1.y + element1.height - y <= 3) {
					//element1.x += 1; //Yes or no, maybe
					element1.y = y - element1.height;
				} else {
					element1.h_speed = 0;
					element1.x = x + width + 1;
				}
			}
			else {
				element1.y = height + y;
				element1.v_speed = 0;
			}
		}
	}
}
