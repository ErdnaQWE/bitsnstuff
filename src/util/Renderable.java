package util;

import org.newdawn.slick.Image;

public class Renderable {

	public int x;
	public int y;
	public Image image;
	
	public Renderable(int x, int y, Image image) {
		
		this.x = x;
		this.y = y;
		this.image = image;
		
	}
	
}
