package util;

import org.newdawn.slick.Graphics;

import instances.Instance;

public interface Solid {

	public void collisionInstance(Instance element1, int delta);
	
	public void render(Graphics g);
	
	public void render(Graphics g, float offsetx, float offsety);
	
	public boolean hitTest(Box b);
	
	public boolean pixelHitTest(float relatvieTargetX, float relativeTargetY);
}
