package util;

import org.newdawn.slick.Graphics;

import instances.Instance;


public class Slope extends Box implements Solid {

	public boolean angle; // false for going up towards right.
	//static final Shape shape = new Polygon(new float[]{0,15,15,15,15,0});
	
	public Slope() {
	}
	
	public Slope(float x, float y, float width, float height, boolean angle) {
		super(x, y, width, height);
		this.angle = angle;
	}
	
	public Slope(float x, float y, float width, float height) {
		super(x, y, width, height);
		this.angle = false;
	}
	
	public void render(Graphics g, float offsetx, float offsety) {
		final float finalX = offsetx + x; // Pour eviter de refaire le calcul des dizaines de fois
		final float finalY = offsety + y;
		if (angle) {
			g.drawLine(finalX, finalY, finalX + width, finalY + height); // Hypotenuse
			g.drawLine(finalX, finalY, finalX, finalY + height); // Cote 1
			g.drawLine(finalX, finalY + height, finalX + width, finalY + height); // Cote 2
		} else {
			g.drawLine(finalX, finalY + height, finalX + width, finalY); // Hypotenuse
			g.drawLine(finalX + width, finalY, finalX + width, finalY + height); // Cote 1
			g.drawLine(finalX, finalY + height, finalX + width, finalY + height); // Cote 2
		}
		
		// super.render(g, offsetx, offsety);
		
	}
	
	public void render(Graphics g) {
		this.render(g,0,0);
	}
	
	public boolean pixelHitTest(float relativeTargetX, float relativeTargetY) {
		
		int normalizedX = (int) Math.floor(relativeTargetX);
		int normalizedY = (int) Math.floor(relativeTargetY);
		
		if (normalizedX > width || normalizedY > height || normalizedX < 0 || normalizedY < 0) return false; // Not within the shape itself
		
		if (angle) {
			return normalizedX > normalizedY;
		} else {
			return normalizedX < normalizedY;
		}
	}
	
	public void collisionInstance(Instance element1, int delta) {
		
		if (hitTest(element1)) { // Verifie pour une collision (dans le carde de la pente)
						
			if (angle) { // Up towards left
				
				float temp = Math.min(getEndX() - element1.x, 32); // Amount of pixels "in" the shape				
				
				if (element1.getEndY() > getEndY() + 2 && element1.getCenterX() > getEndX()) { // Hit the left side (act like a solid block)
					super.collisionInstance(element1, delta);
				} else if (element1.getCenterX() < x && element1.getEndY() > y) { // The instance should just have hit the side of the slope...
					super.collisionInstance(element1, delta);
				} else if (element1.getEndY() > getEndY() - temp && element1.getEndY() < getEndY() + 1) {
					element1.y = getEndY() - temp - element1.height;
					element1.v_speed = 0;
					element1.jumpReady = true;
				}
				
			} else { // Up towards right (normal)
				float temp = Math.min(element1.getEndX() - x, 32); // Amount of pixels "in" the shape
				if (element1.getEndY() > getEndY() + 1 && element1.getCenterX() < x) {
					super.collisionInstance(element1, delta);
				} else if (element1.getCenterX() > getEndX() && element1.getEndY() > y + 1) { // The instance should just have hit the side of the slope...
					super.collisionInstance(element1, delta);
				} else if (element1.getEndY() > getEndY() - temp && element1.getEndY() < getEndY() + 1) {
					element1.y = getEndY() - temp - element1.height;
					element1.v_speed = 0;
					element1.jumpReady = true;
				}
			}
		}
	}
}

