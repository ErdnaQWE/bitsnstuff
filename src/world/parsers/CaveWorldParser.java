package world.parsers;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.newdawn.slick.Image;
import org.newdawn.slick.SpriteSheet;

import engine.Parameters;
import engine.Resources;
import instances.Instance;
import util.Box;
import util.Slope;
import util.Solid;
import world.layers.ParallaxLayer;
import world.layers.TileLayer;

public class CaveWorldParser implements IWorldParser {

	private int mapWidth;
	private int mapHeight;
	
	private Image[] imageArray;
	
	private int solidsFirstGID; // First GID for solids since they are not sprites
		
	public CaveWorldParser(int mapWidth, int mapHeight, JSONArray tilesetArray) {
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		
		parseImageListFromTilesetArray(tilesetArray); // Prepare all the images that could potentially be needed to parse this map
	}
	
	private TileLayer parseLayer(JSONArray data) {
		
		Image[][] parsedImages = new Image[mapWidth][mapHeight];

		int index;

		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				index = Math.max(((Long) data.get((y * mapWidth) + x)).intValue(), 0);
				if (index != 0) {
					parsedImages[x][y] = imageArray[index];
				}
			}
		}

		return new TileLayer(parsedImages);
	}
	
	// TODO: Rework this, redundant code, flag in layer to enable parallax?
	@Override
	public TileLayer parseParallax(JSONArray data) {
		Image[][] parsedImages = new Image[mapWidth][mapHeight];

		int index;

		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				index = Math.max(((Long) data.get((y * mapWidth) + x)).intValue(), 0);
				if (index != 0) {
					parsedImages[x][y] = imageArray[index];
				}
			}
		}

		return new ParallaxLayer(parsedImages, 0.7f, 1f);
	}

	@Override
	public TileLayer parseForeground(JSONArray data) {
		return parseLayer(data);
	}

	@Override
	public TileLayer parseBackground(JSONArray data) {
		return parseLayer(data);
	}

	@Override
	public Solid[][] parseSolids(JSONArray data) {
		int index;
		Solid[][] solids = new Box[mapWidth][mapHeight];
	
		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++) {
				index = ((Long) data.get((y * mapWidth) + x)).intValue() - solidsFirstGID + 1;
				if (index > 0 && index <= 16) { // Super arbitraire
					solids[x][y] = new Box(x * Parameters.SIZE, (y * Parameters.SIZE) + 32 - (index * 2), Parameters.SIZE, Parameters.SIZE);
				} else if (index == 17 || index == 18) {
					solids[x][y] = new Slope(x * Parameters.SIZE, y * Parameters.SIZE, Parameters.SIZE, Parameters.SIZE, index == 18);
				}
			}
		}
		
		return solids;
	}

	@Override
	public ArrayList<Instance> parseInstances(JSONArray data) {
		return new ArrayList<Instance>(); // Not done yet hehe
	}
	
	private void parseImageListFromTilesetArray(JSONArray arr) {
		imageArray = new Image[1];
		JSONObject tileset;
		int firstGID;
		int tileCount;
		String tilesetName; // Should also be the name of the sprite from the Resources, would make things a lot easier
		
		for(int i = 0; i < arr.size(); i++) {
			tileset = (JSONObject) arr.get(i);
			firstGID = Math.round((Long) tileset.get("firstgid"));
			tileCount = Math.round((Long) tileset.get("tilecount"));
			tilesetName = (String) tileset.get("name");
			
			if (tilesetName.toLowerCase().contains("solid")) {
				solidsFirstGID = firstGID;
				continue;
			}
			else if (firstGID + tileCount > imageArray.length) {
				expandImageArray(firstGID + tileCount);
			}
			
			for (int ii = 0; ii < tileCount; ii++) {
				imageArray[firstGID + ii] = getSpriteImage(tilesetName, ii);
			}
		}
	}
	
	private static Image getSpriteImage(String tileset, int index) {		
		SpriteSheet sheet = Resources.getSprite(tileset);
		//int vertical = sheet.getVerticalCount(); // Unused for now, see below
		int horizontal = sheet.getHorizontalCount();
		
		int y = (index / horizontal); // The horizontal on this line was orignaly vertical. In order for the calculations to be correct using vertical, the tileset has to be a square. Using horizontal permits any rectangle displacement to also be used. Merci -Moustache
		int x = (index % horizontal);
		
		return sheet.getSubImage(x, y);
	}
	
	private void expandImageArray(int newSize) {
		Image[] newArray = new Image[newSize];
		System.arraycopy(imageArray, 0, newArray, 0, imageArray.length);
		
		imageArray = newArray;
	}
}
