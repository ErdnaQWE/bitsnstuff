package world.parsers;

import java.util.ArrayList;

import org.json.simple.JSONArray;

import instances.Instance;
import util.Solid;
import world.layers.TileLayer;

public interface IWorldParser {
	
	TileLayer parseForeground(JSONArray arr);
	TileLayer parseBackground(JSONArray arr);
	TileLayer parseParallax(JSONArray arr);
	
	Solid[][] parseSolids(JSONArray arr);
	ArrayList<Instance> parseInstances(JSONArray arr);
}
