package world.parsers;

import java.io.InputStreamReader;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import world.World;

public class WorldFactory {

	// Parse a string and return the associated World Parser
	// This will allow someone building a map to specify which parser to use with a simple string
	
	
	public static IWorldParser parseString(String parserName, int width, int height, JSONArray firstGIDs) {
		String normalizedParserName = parserName.toUpperCase();
		
		if (!normalizedParserName.equals("CAVE")) {
			throw new UnsupportedOperationException("Parser " + parserName + " (normalized to " + normalizedParserName + ") does not exist" );
		} else {
			return new CaveWorldParser(width, height, firstGIDs); // Generic parser built for the initial CaveMap, most maps should work with this
		}
	}
	
	/**
	 * This method reads the JSON file located at "path" and initializes the specified world.
	 * In order to easily read from a JSON file, this method incorporates JSONSimple functions.
	 * 
	 * @param path
	 * 			The relative file path of the JSON file 
	 * @throws Exception
	 * 			Throws an exception if the map could not be properly read.
	 */
	public World load(String path) throws Exception {
		
		World newWorld = new World();
		
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(new InputStreamReader(getClass().getResourceAsStream(path)));
		JSONObject jObj = (JSONObject) obj;
		
		JSONObject mapProperties = (JSONObject) jObj.get("properties");
		newWorld.areaName = (String) mapProperties.get("Area Name");
		newWorld.environment = (String) mapProperties.get("Environment");
		String parserFunction = (String) mapProperties.get("Parser"); // The parser to use

		try {
			newWorld.areaLevel = Byte.parseByte(mapProperties.get("Area Level").toString());
		} catch (NumberFormatException e) { // Redefine the default area level in case parsing was not sucessful
			newWorld.areaLevel = -1;
		}
		
		// If loading was not sucessful, insert default values
		if (newWorld.areaName == null) {
			newWorld.areaName = "???";
		}
		if (newWorld.environment == null) {
			newWorld.environment = "Void";
		}
		
		JSONArray layers = (JSONArray) jObj.get("layers");
		JSONArray tilesets = (JSONArray) jObj.get("tilesets");
				
		int amount = layers.size();
		
		for (int i = 0; i < amount; i++) {
			
			JSONObject layer = (JSONObject) layers.get(i);
			String type = (String) layer.get("name");
			
			if (newWorld.WIDTH == -1) newWorld.WIDTH = ((Long) layer.get("width")).intValue(); // Weird solution to the orignal problem, but it works
			if (newWorld.HEIGHT == -1) newWorld.HEIGHT = ((Long) layer.get("height")).intValue();
			
			IWorldParser worldParser = parseString(parserFunction, newWorld.WIDTH, newWorld.HEIGHT, tilesets);
						
			if (type.toLowerCase().contains("foreground")) {
				newWorld.foregroundLayers.add(worldParser.parseForeground((JSONArray) layer.get("data")));
			} else if (type.toLowerCase().contains("background")) {
				newWorld.backgroundLayers.add(worldParser.parseBackground((JSONArray) layer.get("data")));
			} else if (type.toLowerCase().contains("parallax")) {
				newWorld.backgroundLayers.add(worldParser.parseParallax((JSONArray) layer.get("data")));
			} else if (type.toLowerCase().contains("solids")) {
				newWorld.solids = worldParser.parseSolids((JSONArray) layer.get("data"));
			} else if (type.toLowerCase().contains("enemy")) {
				newWorld.instances.addAll(worldParser.parseInstances((JSONArray) layer.get("data")));
			}
		}
		
		return newWorld;
	}
	
	
	
}
