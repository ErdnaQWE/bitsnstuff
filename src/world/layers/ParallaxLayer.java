package world.layers;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import engine.Parameters;

public class ParallaxLayer extends TileLayer {
	
	private float xMoveAdj;
	private float yMoveAdj;
	
	public ParallaxLayer(Image[][] tiles, float xMoveAdj, float yMoveAdj) {
		super(tiles);
		
		this.xMoveAdj = xMoveAdj;
		this.yMoveAdj = yMoveAdj;
		
		this.xOrigin = 0; // TEMP!!!
		this.yOrigin = 0;
	}
	
	@Override
	public void render(Graphics g, int xPosAdj, int yPosAdj, int minRenderX, int maxRenderX, int minRenderY, int maxRenderY) {
		
		int parallaxXOffset = Math.round(xOrigin + (xPosAdj * xMoveAdj));
		int parallaxYOffset = Math.round(yOrigin + (yPosAdj * yMoveAdj));
		
		int tileXAdjust = (parallaxXOffset - xPosAdj) / Parameters.SIZE;
		int tileYAdjust = (parallaxYOffset - yPosAdj) / Parameters.SIZE;
		
		/*System.out.println((minRenderX + tileXAdjust - (maxRenderX - tileXAdjust)));
		System.out.println((minRenderY + tileYAdjust - (maxRenderY - tileYAdjust)));*/
		
		super.render(g, parallaxXOffset, parallaxYOffset, minRenderX + tileXAdjust - 1, maxRenderX - tileXAdjust, minRenderY + tileYAdjust - 1, maxRenderY - tileYAdjust);	
	}
}
