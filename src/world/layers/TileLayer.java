package world.layers;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import engine.Parameters;

public class TileLayer {
	
	protected Image[][] tiles;
	
	private static float renderScale = (float) Parameters.SIZE / Parameters.ORIGINAL_SIZE;	
	
	protected float xOrigin;
	protected float yOrigin;
	
	public TileLayer(int width, int height) {
		this(new Image[width][height]);
	}
	
	public TileLayer(Image[][] tiles) {
		this.tiles = tiles;
	}
	
	public void update(int delta) {
		// Do nothing
	}
	
	public void render(Graphics g, int xPosAdj, int yPosAdj, int minRenderX, int maxRenderX, int minRenderY, int maxRenderY) {
		for (int x = Math.max(0, minRenderX); x < tiles.length && x < maxRenderX ; x++) {
			for (int y = Math.max(0, minRenderY); y < tiles[x].length && y < maxRenderY; y++) {
				if (tiles[x][y] != null) { // Drawing a null tile makes the game crash
					g.drawImage(tiles[x][y].getScaledCopy(renderScale), x * Parameters.SIZE - xPosAdj + xOrigin, y * Parameters.SIZE - yPosAdj + yOrigin);
				}
			}
		}
	}
	
	public void setOrigin(float xOrigin, float yOrigin) {
		this.xOrigin = xOrigin;
		this.yOrigin = yOrigin;
	}
}
