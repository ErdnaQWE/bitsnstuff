package world;

import engine.Parameters;
import engine.Window;
import instances.Instance;

public class Camera {
	
	public int maxRenderX;  // NOSONAR, but should probably fix
	public int minRenderX;  // NOSONAR, but should probably fix
	public int maxRenderY;  // NOSONAR, but should probably fix
	public int minRenderY;  // NOSONAR, but should probably fix
	
	public int xPosAdj;  // NOSONAR, but should probably fix
	public int yPosAdj;  // NOSONAR, but should probably fix
	
	public float xPos;  // NOSONAR, but should probably fix
	public float yPos;  // NOSONAR, but should probably fix
	
	private static final float CAMERA_SPEED = 2f;
	private static final float CAMERA_X_PADDING = 600f;  // These two could need a little modification depending on the screen resolution
	private static final float CAMERA_Y_PADDING = 250f;
	
	private Instance instance;
	private World world;
	
	public Camera(Instance associatedInstance, World associatedWorld) {
		this.instance = associatedInstance;
		this.world = associatedWorld;
		
		this.xPos = instance.x;
		this.yPos = instance.y;
	}
	
	public void update(int delta) {
		if (instance.x > xPos + Window.getWidth() - CAMERA_X_PADDING) { //Approaches right side of the screen
			if (xPos < world.WIDTH * Parameters.SIZE) { //
				xPos += delta/1000.0 * (( instance.x - (xPos + Window.getWidth() - CAMERA_X_PADDING)) * CAMERA_SPEED);
			}
			if (xPos + Window.getWidth() >= world.WIDTH * Parameters.SIZE) { // If the camera leaves the gameplay area...
				xPos = world.WIDTH * Parameters.SIZE - Window.getWidth(); // ...we move it back.
			}
		} else if (instance.x < xPos + CAMERA_X_PADDING) { //Approaches left side of the screen
			if (xPos >= 0) {
				xPos -= delta/1000.0 * (( xPos + CAMERA_X_PADDING - instance.x ) * CAMERA_SPEED); //Fix this?
			}
			if (xPos < 0) { // If the camera leaves the gameplay area...
				xPos = 0; // ...we move it back.
			}
		}
		
		if (instance.y > yPos + Window.getHeight() - CAMERA_Y_PADDING) { //Approaches bottom of the screen
			if (yPos < world.HEIGHT * Parameters.SIZE) {
				yPos += delta/1000.0 * ((instance.y - (yPos + Window.getHeight() - CAMERA_Y_PADDING)) * CAMERA_SPEED);
			}
			if (yPos + Window.getHeight() >= world.HEIGHT * Parameters.SIZE) { // If the camera leaves the gameplay area...
				yPos = world.HEIGHT * Parameters.SIZE - Window.getHeight(); // ...we move it back.
			}
		} else if (instance.y < yPos + CAMERA_Y_PADDING) { //Approaches top of the screen
			if (yPos >= 0) {
				yPos -= delta/1000.0 * ((yPos + CAMERA_Y_PADDING - instance.y) * CAMERA_SPEED);
			}
			if (yPos < 0) { // If the camera leaves the gameplay area...
				yPos = 0; // ...we move it back.
			}
		}
		
		minRenderX = (int) (Math.floor(xPos / Parameters.SIZE));  // First horizontal tile to render
		maxRenderX = (int) (Math.ceil((Window.getWidth() + xPos) / Parameters.SIZE));  // Last horizontal tile to render
		minRenderY = (int) (Math.floor(yPos / Parameters.SIZE));  // First horizontal tile to render
		maxRenderY = (int) (Math.ceil((Window.getHeight() + yPos) / Parameters.SIZE));  // Last horizontal tile to render
		
		xPosAdj = Math.round(xPos);  // In order to avoid the white lines, the position rounding is done once and for all elements
		yPosAdj = Math.round(yPos);
	}
}
