package world;

import instances.Instance;
import instances.Light;
import instances.Instance.INSTANCE_TAG;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import util.Solid;
import world.layers.TileLayer;

public class World {
	
	//Information
	public String areaName; // The name of the area that will be displayed in-game
	public int areaLevel; // The level of the area that will be used for multiple calculations in-game.
	public String environment; // The environment type that will be used in-game.
	
	//World
	public List<TileLayer> foregroundLayers = new ArrayList<TileLayer>();
	public List<TileLayer> backgroundLayers = new ArrayList<TileLayer>();
	public Solid[][] solids; // Anything that could come in contact with Bit, or any other instance.
	
	//Instances
	public List<Instance> instances = new ArrayList<Instance>(); // All of the instances (mostly anything that cannot be interacted with)
	public List<Instance> frontInstances = new ArrayList<Instance>(); // All of the instances the should appear in front of everything (this should be particles and such)
	public List<Light> lights = new ArrayList<Light>();
	public List<Solid> extraSolids = new ArrayList<Solid>(); // All of the extra solids on which instances stand on, should mostly remain empty as the blocks are placed in "Solid[][] solids" when parsing
	
	public int WIDTH = -1;
	public int HEIGHT = -1;
	
	public boolean debugDrawMode = false;
	
	public void update(int delta) {
		for (int i = 0; i < instances.size(); i++) {
			instances.get(i).update(delta);
		}
		
		for (int i = 0; i < frontInstances.size(); i++) {
			frontInstances.get(i).update(delta);
		}

		Instance currentInstance;
		for (int i = 0; i < instances.size(); i++) {
			currentInstance = instances.get(i);
			if (currentInstance.tags.contains(INSTANCE_TAG.TANGIBLE)) {
				treatSolidContactsNearestInWorld(instances.get(i), delta);
			}
		}

		Instance externalInstance;
		Instance internalInstance;
		for (int i = 0; i < instances.size(); i++) {
			externalInstance = instances.get(i);
			for (int ii = i; ii < instances.size(); ii++) {
				internalInstance = instances.get(ii);
				
				if (externalInstance != internalInstance && externalInstance.hitTest(internalInstance)) { // We wouldn't want the instance to collide with itself hehe
					externalInstance.contact(internalInstance);
					internalInstance.contact(externalInstance);
				}
			}
		}
		
		removeInstances(instances);
		removeInstances(frontInstances);
	}
	
	public void render(Graphics g, Camera camera) {
		renderBackgroundLayers(g, camera);
		renderInstances(g, camera);
		renderForegroundLayers(g, camera);
		renderFrontInstances(g, camera);
		
		if (debugDrawMode) renderSolids(g, camera); 
	}
	
	private void renderForegroundLayers(Graphics g, Camera camera) {
		for (int i = 0; i < foregroundLayers.size(); i++) {
			foregroundLayers.get(i).render(g, camera.xPosAdj, camera.yPosAdj, camera.minRenderX, camera.maxRenderX, camera.minRenderY, camera.maxRenderY);
		}
	}
	
	private void renderBackgroundLayers(Graphics g, Camera camera) {
		for (int i = 0; i < backgroundLayers.size(); i++) {
			backgroundLayers.get(i).render(g, camera.xPosAdj, camera.yPosAdj, camera.minRenderX, camera.maxRenderX, camera.minRenderY, camera.maxRenderY);
		}
	}
	
	private void renderInstances(Graphics g, Camera camera) {
		for (int i = 0; i < instances.size(); i++) {
			instances.get(i).render(g, -camera.xPosAdj, -camera.yPosAdj);
		}
	}
	
	private void renderFrontInstances(Graphics g, Camera camera) {
		for (int i = 0; i < frontInstances.size(); i++) {
			frontInstances.get(i).render(g, -camera.xPosAdj, -camera.yPosAdj);
		}	
	}
	
	private void renderSolids(Graphics g, Camera camera) {
		g.setColor(Color.green);
		for (int x = Math.max(camera.minRenderX,0); x < solids.length && x < camera.maxRenderX; x++) {
			for (int y = Math.max(camera.minRenderY,0); y < solids[x].length && y < camera.maxRenderY; y++) {
				if (solids[x][y]!= null) {
					solids[x][y].render(g,-camera.xPosAdj,-camera.yPosAdj);
				}
			}
		}
	}

	// Remove all instances that are ready to be removed
	private void removeInstances(List<Instance> instanceList) {
		
		Iterator<Instance> iterator = instanceList.iterator();
		Instance currentInstance;
		
		while(iterator.hasNext()) {
			currentInstance = iterator.next();
			
			if (currentInstance.isRemovable()) iterator.remove();
		}
	}
	
	/**
	 * 
	 * @param element
	 * @param delta
	 */
	private void treatSolidContactsNearestInWorld (Instance element, int delta) {
		for (int x = Math.max(0, element.firstXPositionInWorld()); x < Math.min(solids.length, element.lastXPositionInWorld()); x++) {
			for (int y = Math.max(0, element.firstYPositionInWorld()); y < Math.min(solids[x].length, element.lastYPositionInWorld()); y++) {
				if (solids[x][y]!= null) {
					Solid element2 = solids[x][y]; // Currently treated instance

					element2.collisionInstance(element, delta);  // Collisions for the element
				}
			}
		}
	}
	
	/**
	 * Give a point and get a reference to the solid that is currently residing at that point
	 * 
	 * @param target x coordinate
	 * @param target y coordinate
	 * @return The solid at the specified point, null if the point is empty
	 */
	public Solid getSolidAtPoint(float targetX, float targetY) {
		int arrayX = (int) Math.floor(targetX / 32);
		int arrayY = (int) Math.floor(targetY / 32);
		
		Solid foundSolid = null;
		try {
			foundSolid = solids[arrayX][arrayY];
		} catch (ArrayIndexOutOfBoundsException ex) {
			System.err.println("Couldn't find a solid at point [" + arrayX + ", " + arrayY + "]");
		}
		
		return foundSolid;
	}
}
