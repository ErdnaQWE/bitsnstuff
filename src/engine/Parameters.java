package engine;

public class Parameters {

	// CONSTANTS
	
	public static final int SIZE = 32; // Game size, 32 is x2 of the original sprites sizes.
	public static final int ORIGINAL_SIZE = 16;
	
	// Once and for all, every original image file should be 1:1 pixels. They will be pixel doubled in software at load time or at game time depending on the need
	
	public static final boolean LIGHTING_ENABLED = false; // Will be changed to a variable later
	
	public static final String INITIAL_MAP = "NewerCave.json"; // The first map to load when the game launches.
	
	public static final String GAME_VERSION = "Alpha 9";
	
}
