/********************
 * Bits & Stuff
 * Java Version
 * By: Alexandre Dion & Vincent Gascon
 ********************/

package engine;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;

import states.*;

public class Engine extends StateBasedGame {

	public Engine() {
		super("Bits & Stuff | PROTOTYPE");
	}
	
	public static void main (String[] args) {
		
		System.out.println("BITS & STUFF");
		System.out.println("Alpha Version");
		System.out.println("By: Mustache Man");
		
		try {
			AppGameContainer game = new AppGameContainer(new Engine());
			Window.setContainer(game);
			
			String[] listOfIcons = {"res/icons/icon16.gif", "res/icons/icon32.gif"};
			game.setIcons(listOfIcons);
			
			/*DisplayMode[] modes = Window.getDisplayModes();
			
			System.out.println("Listing display modes:...");
			for(int i = 0; i < modes.length; i++) {
				System.out.println(modes[i].toString());
			}*/
			
			Window.setDisplayMode(4); // TODO BIG NO NO!!! JUST FOR DEBUGGING!
			game.start();
			
		} catch (SlickException e) {
			e.printStackTrace();
			System.out.println("Error in initialisation.");
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		//gc.setTargetFrameRate(60);
		gc.setMaximumLogicUpdateInterval(1000/20); // If the game slows down further than 20 FPS, then I don't know what will happen
		gc.setMinimumLogicUpdateInterval(1000/120); // Put a cap to save my cpu while coding // Until further testing, there should not be a cap on FPS, the game should be able to update as fast as it can (as long as floating points allow it)
		//gc.setVSync(true);
		gc.setAlwaysRender(true);
		gc.setShowFPS(true); //Shown later in the debug information
		
		new Resources(); //Load resources
		
		this.addState(new PlayState());
		//this.addState(new GameIntroState());
		
	}

}
