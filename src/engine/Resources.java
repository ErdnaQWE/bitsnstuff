package engine;

import java.awt.Font;
import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
// import org.newdawn.slick.Sound; // Not for now
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

public class Resources {
	
	private static Map<String, Image> images;
	private static Map<String, SpriteSheet> sprites;
	private static Map<String, fonts.Font> fonts;
	//private static Map<String, Sound> sounds; //NOSONAR
	
	public static TrueTypeFont LucidaBlackletter;
	
	public Resources() {
		images = new HashMap<String, Image>();
		sprites = new HashMap<String, SpriteSheet>();
		fonts = new HashMap<String, fonts.Font>();
		//sounds = new HashMap<String, Sound>(); //NOSONAR
		
		try { //Chargement de chaque image en specifiant sa location dans le dossier res (ressources)
			// sprites.put("bit_walk", loadSprite("res/images/bit/bit_walk.png", 21, 39)); //Bit walking animation (14 frames)
			
			sprites.put("bit_run", loadSprite("res/images/bit/bit_run.png", 64, 64)); // Bit running animation (Loops at frame 5)
			sprites.put("bit_run_knives", loadSprite("res/images/bit/knives/bit_run_knives.png", 64, 64));
			
			//images.put("bit_stand", loadImage("res/images/bit/bit_stand.png")); //DEBUG (Just so I don't have to look at a red rectangle all the time now)
			sprites.put("bit_stand", loadSprite("res/images/bit/bit_idle_noarms.png", 64, 64));
			sprites.put("bit_stand_shrug", loadSprite("res/images/bit/bit_idle.png", 64, 64));
			sprites.put("bit_idle_knives", loadSprite("res/images/bit/knives/bit_idle_knives.png", 64, 64));

			sprites.put("bit_jump", loadSprite("res/images/bit/bit_jump.png", 40, 64)); //Bit jumping animation
			sprites.put("bit_swing", loadSprite("res/images/bit/bit_swing.png", 66, 64)); //Bit
			sprites.put("bit_teleslash", loadSprite("res/images/bit/knives/bit_teleslash.png", 128, 64)); // Knives slash + teleport
			
			sprites.put("bit_ledge", loadSprite("res/images/bit/bit_ledge.png",40, 64)); // Bit grabbing the ledge (9 frames)
			
			// TILESETS
			sprites.put("cave_foreground", loadSprite("res/images/tilesets/cave_old/cave_foreground.png", 32, 32));
			sprites.put("cave_background", loadSprite("res/images/tilesets/cave_old/cave_background.png", 32, 32));
			
			sprites.put("cave_foreground_1", loadSprite("res/images/tilesets/cave/cave_foreground.png",32,32));
			sprites.put("cave_background_1", loadSprite("res/images/tilesets/cave/cave_background.png",32,32));
			
			sprites.put("Cave_Floor", loadSprite("res/images/tilesets/cave/Cave_Floor.png",16,16));
			sprites.put("Cave_Bottom", loadSprite("res/images/tilesets/cave/Cave_Bottom.png",16,16));
			sprites.put("CaveTileset", loadSprite("res/images/tilesets/cave/CaveTileset.png",16,16));
			sprites.put("CaveParallaxTileset", loadSprite("res/images/tilesets/cave/CaveParallaxTileset.png",16,16));

			
			// ENEMIES
			sprites.put("MoleWalk", loadSprite("res/images/enemies/mole/MoleWalk.png",64,64));
			//images.put("mole", loadImage("res/images/enemies/mole.png"));
			
			// LOGO TEST
			images.put("0", loadImage("res/images/misc/0.png"));
			images.put("1", loadImage("res/images/misc/1_alt.png"));
			images.put("B", loadImage("res/images/misc/letters/B.png"));
			
			// FONTS
			fonts.put("BeeEnEss", loadFont("BeeEnEss"));
			
			InputStream inputStream	= ResourceLoader.getResourceAsStream("res/fonts/LucidaBlackletter.TTF");
			
			Font awtFont2 = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont2 = awtFont2.deriveFont(24f); // set font size
			awtFont2 = awtFont2.deriveFont(Font.BOLD);
			LucidaBlackletter = new TrueTypeFont(awtFont2, false);
		}
		catch (SlickException e) {
			System.err.println("Could not load all required ressources.");
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace(); // TODO LAZY
		}
		
	}

	private static Image loadImage (String path) throws SlickException {
		return new Image (path, false, Image.FILTER_NEAREST);  //Charge une image d'un fichier 
	}
	
	public static SpriteSheet getSprite(String getter) {
		return sprites.get(getter);
	}
	
	public static Image getSpriteImage(String getter, int x, int y) {
		return sprites.get(getter).getSubImage(x, y);
	}
	
	public static Image getImage (String getter) {
		return images.get(getter);
	}
	
	public static Image image(String getter) {
		return sprites.get(getter);
	}
	
	public static Image getSprite (String getter, int x, int y) {
		return sprites.get(getter).getSprite(x, y);
	}
	
	private static SpriteSheet loadSprite (String path, int tw, int th) throws SlickException {
		return new SpriteSheet (loadImage(path), tw, th); //Charge un sprite d'un fichier image
	}
	
	public static fonts.Font getFont(String fontName) {
		return fonts.get(fontName);
	}
	
	// This should probably be moved somewhere else at some point
	// also, refactor the name thing
	private static fonts.Font loadFont(String fontName) {
		Image[] characters = new Image[109384];
		
		File folder = new File("res/fonts/" + fontName);
		File[] listOfFiles = folder.listFiles();
		
		for (int i = 0; i < listOfFiles.length; i++) {
			try {
				if (listOfFiles[i].isFile() && !listOfFiles[i].getName().substring(0, 2).equals("._")) {
					int unicodeNumber = Integer.parseInt(listOfFiles[i].getName().replace(".png",""));
					
					characters[unicodeNumber] = loadImage(listOfFiles[i].getAbsolutePath());
				}
			}
			catch (Exception ex) {
				System.out.println("Could not load file " + listOfFiles[i].getName());
			}
		}
		
		fonts.Font resultingFont = new fonts.Font();
		resultingFont.loadFont(characters);
		
		return resultingFont;
	}
}
