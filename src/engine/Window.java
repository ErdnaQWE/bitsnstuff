//Contains the default/hardcoded window settings for the game

package engine;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Window {
	
	private static int width = 800;
	private static int height = 600;
	private static boolean isFullscreen = false;
	
	private static AppGameContainer container;
	private static int currentMode; //TODO THIS IS FOR DEBUG ONLY, REMOVE THIS EVENTUALLY BECAUSE THINGS ARE GOING TO BREAK...
	
	public static void setContainer(AppGameContainer appGameContainer) {
		container = appGameContainer;
	}
	
	public static void setDisplayMode(DisplayMode displayMode) {
		try {
			width = displayMode.getWidth();
			height = displayMode.getHeight();
			container.setDisplayMode(width, height, isFullscreen);
		} catch (SlickException e) {
			System.err.println("Could not switch to displayMode: " + displayMode.toString()); //TODO Better handling of the exception?
			e.printStackTrace();
		}
	}
	
	public static void setDisplayMode(int modeNumber) {
		currentMode = modeNumber % getDisplayModes().length;
		setDisplayMode(getDisplayModes()[modeNumber]);
	}
	
	public static void setFullscreen(Boolean fullscreen) {
		isFullscreen = fullscreen;
	}
	
	//Getters
	
	public static int getWidth() {
		return container.getWidth();
	}
	
	public static int getHeight() {
		return container.getHeight();
	}
	
	public static boolean isFullscreen() {
		return isFullscreen;
	}
	
	public static int getWidthMiddle() {
		return getWidth() / 2;
	}
	
	public static int getHeightMiddle() {
		return getHeight() / 2;
	}
	
	public static DisplayMode[] getDisplayModes() {
		try {
			return Display.getAvailableDisplayModes();
		} catch (LWJGLException e) {
			System.err.println("Could not get available display modes. Returning null, game might crash.");
			e.printStackTrace();
			return null;
		}
	}
	
	public static int getCurrentModeNumber() {
		return currentMode;
	}
	
}
