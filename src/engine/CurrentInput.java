package engine;

import org.newdawn.slick.Input;

public class CurrentInput {

	private KeyboardMap keyboardMap;
	private Input input;
	
	public boolean jumpButton;
	
	public boolean upMoveButton;
	public boolean downMoveButton;
	public boolean leftMoveButton;
	public boolean rightMoveButton;
	
	public boolean upActionButton;
	public boolean downActionButton;
	public boolean leftActionButton;
	public boolean rightActionButton;
	
	public CurrentInput(KeyboardMap keyboardMap, Input input) {
		this.keyboardMap = keyboardMap;
		this.input = input;
		
		reset();
	}
	
	public void update() {
		upMoveButton = input.isKeyDown(keyboardMap.upMovement);
		downMoveButton = input.isKeyDown(keyboardMap.downMovement);
		leftMoveButton = input.isKeyDown(keyboardMap.leftMovement);
		rightMoveButton = input.isKeyDown(keyboardMap.rightMovement);
		
		upActionButton = input.isKeyDown(keyboardMap.upAttack);
		downActionButton = input.isKeyDown(keyboardMap.downAttack);
		leftActionButton = input.isKeyDown(keyboardMap.leftAttack);
		rightActionButton = input.isKeyDown(keyboardMap.rightAttack);
		
		jumpButton = input.isKeyDown(keyboardMap.upMovement);
	}
	
	private void reset() {
		jumpButton = false;
		
		upMoveButton = false;
		downMoveButton = false;
		leftMoveButton = false;
		rightMoveButton = false;
		
		upActionButton = false;
		downActionButton = false;
		leftActionButton = false;
		rightActionButton = false;
	}
	
	public Input getInputObject() {
		return input;
	}
	
}
