package engine;

import org.newdawn.slick.Input;

public class KeyboardMap {
	
	// Movmement
	public int upMovement = Input.KEY_W;
	public int leftMovement = Input.KEY_A;
	public int downMovement = Input.KEY_S;
	public int rightMovement = Input.KEY_D;
	
	// Attack
	public int upAttack = Input.KEY_UP;
	public int leftAttack = Input.KEY_LEFT;
	public int downAttack = Input.KEY_DOWN;
	public int rightAttack = Input.KEY_RIGHT;
	
	// Other Game
	public int openInventory = Input.KEY_I;
	
	// Technical
	public int toggleFullscreen = Input.KEY_F;
	public int toggleDebug = Input.KEY_F1;
	public int toggleSlowmotion = Input.KEY_Y;
}
