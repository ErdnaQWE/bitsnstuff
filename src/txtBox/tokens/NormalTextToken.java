/* B & S : NormalTextToken
 * Alexandre Dion : 2017
 */

package txtBox.tokens;

import org.newdawn.slick.Graphics;

import engine.Resources;
import fonts.Font;
import txtBox.IToken;

public class NormalTextToken implements IToken {

	private int visibleLetters;
	private int animationCounter;
	private static final int TIME_PER_LETTER = 100;
	private String value;
	
	private static Font currentFont = Resources.getFont("BeeEnEss");
	
	public NormalTextToken(String value) {
		this.visibleLetters = 0;
		this.value = value;
	}
	
	@Override
	public void update(int delta) {
		if (visibleLetters < value.length()) {
			animationCounter += delta;
			
			if (animationCounter >= TIME_PER_LETTER) {
				animationCounter -= TIME_PER_LETTER;
				visibleLetters += 1;
			}
		}
	}

	@Override
	public void render(Graphics g, int x, int y) {
		// TODO A little fancy animation here once the new inhouse engine is ready
		currentFont.drawString(g, x, y, value.substring(0,visibleLetters));
	}

	@Override
	public int getCurrentWidth() {
		return getFinalWidth(); // Is good for now?
	}
	
	@Override
	public int getFinalWidth() {
		return currentFont.getStringWidth(value) + 6;
	}

	@Override
	public int getHeight() {
		return currentFont.getStringHeight(value); // Is good for now too?
	}

	@Override
	public boolean isDone() {
		return visibleLetters >= value.length();
	}
}
