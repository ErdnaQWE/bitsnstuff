package txtBox.tokens;

import org.newdawn.slick.Graphics;

import txtBox.IToken;

public class PauseToken implements IToken {
	
	private int timeLeftToWait;
	
	public PauseToken(int timeToWait) {
		this.timeLeftToWait = timeToWait;
	}
	
	@Override
	public void update(int delta) {
		if (!isDone()) {
			timeLeftToWait -= delta;
		}
	}
	
	@Override
	public void render(Graphics g, int x, int y) {
		// Nothing, Pauses should be invisible
	}
	
	public int getCurrentWidth() {
		return getFinalWidth();
	}
	
	@Override
	public int getFinalWidth() {
		return 0;  // NOSONAR (wtf)
	}
	
	@Override
	public int getHeight() {
		return 0;  // NOSONAR (wtf)
	}
	
	@Override
	public boolean isDone() {
		return timeLeftToWait <= 0;
	}
	

}
