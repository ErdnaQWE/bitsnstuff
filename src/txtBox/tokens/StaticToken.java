/* B & S : StaticTextToken
 * Alexandre Dion : 2017
 */

package txtBox.tokens;

import org.newdawn.slick.Graphics;

import txtBox.IToken;

public class StaticToken implements IToken {

	private String value;
	
	public StaticToken(String value) {
		this.value = value;
	}
	
	@Override
	public void update(int delta) {
		// Nothing to update
	}

	@Override
	public void render(Graphics g, int x, int y) {
		g.drawString(value, x, y); // TODO Render using inhouse font engine
	}

	@Override
	public int getCurrentWidth() {
		return getFinalWidth();
	}
	
	@Override
	public int getFinalWidth() {
		return value.length() * 12; // TODO Mesure using inhouse font engine
	}

	@Override
	public int getHeight() {
		return 12; // TODO Mesure using inhouse font engine
	}

	public boolean isDone() {
		return true;
	}
	
}
