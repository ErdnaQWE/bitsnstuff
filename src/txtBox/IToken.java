package txtBox;

import org.newdawn.slick.Graphics;

public interface IToken {

	void update(int delta);
	
	void render(Graphics g, int x, int y);
	
	int getCurrentWidth();
	
	int getFinalWidth();
	
	int getHeight();
	
	// Is the token "animation" or "action" done? Is it ok if the SpeechBox goes on to the next token?
	boolean isDone();
	
	// Maybe have a parse function here so each token can be parsed on it's own
	// For example, the pause token takes an amount of time as a parameter. As soon as a Pause token is detected, the Token class could use the pause token parse method to parse everything between the two %.
	// Also look into using regexs for parsing instead of doing it character by character
}
