// Example token, do not use, override methods as this class will probably be turned into an interface at some point

package txtBox;

import java.util.LinkedList;
import txtBox.tokens.*;

public class Token {

	private enum Type {STATIC_TEXT, NORMAL_TEXT, PAUSE}

	public static IToken[][] parseTokens(String[] rawText) {
		/* An overview on how to use tokens
		 *
		 * % marks the start and the end of a token (a list of all possible tokens should exist somewhere
		 *
		 * After the inital % symbol is the name of the token, usually in upper_case letters (although it is not required and should be case insensitive)
		 * After the name are the attributes of the token. Attributes can differ depending on the token. These attributes should be case insensitive if I do my job right.
		 * 	For example, in the case of a PAUSE token, the attribute following it should be the time of the pause in ms.
		 *
		 * The system will get lazy at some points and make guesses.
		 *
		 * Every line of text must start with a token, if there isn't one, the system will default to creating a STATIC_TEXT token.
		 *
		 * White space is usually ignored when defining the token (%PAUSE% is the same as % PAUSE     %)
		 * The first space after a token (if there is one, is ignored). This is in order to make the files easier to read. However, it is not required.
		 *
		 * Here is an example of how dialogue would be made by the end user, because explaining is hard:
		 *
		 * % NORMAL_TEXT % Some normal text that will be split up in normal_text tokens. % PAUSE 1000 % More normal text
		 * 	The line above would make every word appear using the normal_text animation, then pause for one second, then make the rest appear as normal text
		 *
		 * % PAUSE 500 % % NORMAL_TEXT % Some normal text. % PAUSE 500 % Some more normal text
		 * 	The line above would wait half a second before displaying displaying the following text as NORMAL_TEXT. Then wait another half second and the display more NORMAL_TEXT.
		 *
		 * As you can see NORMAL_TEXT is like setting a mode (in fact that is basically how the parser sees it) saying that all text after it should be separated in NORMAL_TEXT tokens.
		 * 	The same thing happens with STATIC_TEXT. You can mix and match the two in the same line:
		 * % PAUSE 500 % % STATIC % Some static text % NORMAL % Some normal text.
		 * 	In this case, after waiting half a second, static text would be displayed and then the normal text
		 *
		 */

		// Maybe create some sort of factory or helper class as an improvement to this class
		// Goal is to make a system that is easily usable by someone who is not familiar with the game code
		// Also, would be nice to store these text strings somewhere else so someone can work on it outside of Eclipse

		IToken[][] resultingTokens;

		// Lists used for parsing lines
		LinkedList<IToken> lineOfTokens; // A line of tokens, eventually converted to an array of Tokens
		LinkedList<IToken[]> listOfLines = new LinkedList<IToken[]>(); // All of the lines, eventually converted to an array
		IToken[] tempArrayToken;

		String detected = "";
		IToken newToken;
		Type newTokenType;
		Type currentTokenMode; // Default
		int value; // Some value used for certain attributes, I declared it here, but there is probably a better way to do this

		for (int i = 0; i < rawText.length; i++) {

			lineOfTokens = new LinkedList<IToken>();
			currentTokenMode = Type.STATIC_TEXT; // Reset to default on new line

			for (int c = 0; c < rawText[i].length(); c++) { // lel

				if (rawText[i].charAt(c) == '%') {

					detected = ""; // Empty the string before the detection of a new token

					c += 1; // Add one so the following loop doesn't stop right away

					while (c < rawText[i].length() && rawText[i].charAt(c) != '%') {
						detected = detected + rawText[i].charAt(c); // Read the token
						c += 1;
					}

					if (c + 1 == rawText[i].length()) {
						continue; // If this is the last character, move onto the next line.
					}

					if (rawText[i].charAt(c + 1) == ' ') {
						c += 1; // Skip the first whitespace, if there is one
					}

					newTokenType = parseType(detected);
					detected = detected.toLowerCase();

					// Some tokens use attributes, they are detected below when required
					if (newTokenType == Type.PAUSE) {
						value = Integer.parseInt(detected.replace("pause", "").replaceAll("\\s+",""));

						newToken = new PauseToken(value);
						lineOfTokens.add(newToken);

					} else {
						currentTokenMode = newTokenType;
					}

				} else {

					detected = "";

					while (c < rawText[i].length()) {						
						if (rawText[i].charAt(c) == ' ') {
							break; // Don't take the space, break the loop to make the token
						} else if (rawText[i].charAt(c) == '%') {
							c -= 1; // Go back one so next loop detects the symbol
							break; // Don't take the symbol, break the loop to make the token
						} else {
							detected = detected + rawText[i].charAt(c);
							c += 1;
						}
					}
					
					switch (currentTokenMode) {

						case NORMAL_TEXT:
							newToken = new NormalTextToken(detected);
							break;

						case STATIC_TEXT:
						default:
							newToken = new StaticToken(detected);
							break;
					}

					lineOfTokens.add(newToken);
				}
			}

			// TODO Not too happy with this, maybe make something better, if possible
			// Check toArray(T[]), pretty sure I'm not using it right somehow
			tempArrayToken = new IToken[lineOfTokens.size()];
			for (int r = 0; r < lineOfTokens.size(); r++) {
				tempArrayToken[r] = lineOfTokens.get(r);
			}

			listOfLines.add(tempArrayToken);
		}

		resultingTokens = new IToken[listOfLines.size()][];
		for(int i = 0; i < listOfLines.size(); i++) {
			resultingTokens[i] = listOfLines.get(i);
		}
		
		return resultingTokens;
		
		// DEBUG
		/*for (int i = 0; i < tokens.length; i++) { // NOSONAR
			for (int ii = 0; ii < tokens[i].length; ii++) {
				System.out.println("Token{" + i + "}{" + ii + "}: " + tokens[i][ii].type + " | " + tokens[i][ii].value + " | " + tokens[i][ii].counter);
			}
 		}*/

	}

	private static Type parseType(String typeName) {

		// typeName.replaceAll("\\s+",""); // Remove all whitespace

		String loweredTypeName = typeName.toLowerCase();

		if (loweredTypeName.contains("pause")) {
			return Type.PAUSE;
		} else if (loweredTypeName.contains("normal")) {
			return Type.NORMAL_TEXT;
		} else {
			return Type.STATIC_TEXT;
		}
	}
}


