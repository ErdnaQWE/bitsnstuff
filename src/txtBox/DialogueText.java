// A speech bubble in which some text is displayed
// It is an instance, for now....

package txtBox;

import org.newdawn.slick.Graphics;

import instances.Instance;

public class DialogueText extends Instance {
	
	private IToken[][] tokens;	// A list of all of the text box's tokens (first dimension is for lines, second is the tokens that make up the line)

	public DialogueText(String[] rawText, float x, float y) {
		
		super(x,y,0,0, null);
		
		tokens = Token.parseTokens(rawText);
	}
	
	@Override
	public void update(int delta) {
		
		// Although not important, the procedure for updating the tokens should be improved so it does not require the use of a label for the loop
		
		out: for(int i = 0; i < tokens.length; i++) {
			for(int ii = 0; ii < tokens[i].length; ii++) {
				tokens[i][ii].update(delta);
				
				if (!tokens[i][ii].isDone()) {
					break out;
				}
			}
		}
	}
	
	@Override
	public void render(Graphics g, float offsetX, float offsetY) {
		
		// This should be updated so it doesn't require the double break at some point
		
		int currentWidth;
		int currentHeight = 0;
		
		int tallestToken;
		
		boolean breakTheLoop = false;
		
		for(int i = 0; i < tokens.length; i++) {
			currentWidth = 0;
			tallestToken = 0;
			
			for(int ii = 0; ii < tokens[i].length; ii++) {
				tokens[i][ii].render(g, (int) Math.floor(x + offsetX + currentWidth), (int) Math.floor(y + offsetY + currentHeight));
				
				currentWidth += tokens[i][ii].getCurrentWidth();
				
				if (tokens[i][ii].getHeight() > tallestToken) {
					tallestToken = tokens[i][ii].getHeight(); // Adjust the lines height for the current tallest token in it
				}
				
				if (!tokens[i][ii].isDone()) {
					breakTheLoop = true;
					break;
				}
			}
			
			if (breakTheLoop) {
				break;
			}
			
			currentHeight += tallestToken + 4; // Add a space in between the lines
		}
	}
}
