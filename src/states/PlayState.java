package states;

import java.util.ArrayList;

import instances.*;
import instances.enemies.Mole;
import instances.others.ExperiencePoint;
import instances.player.Player;
import txtBox.DialogueText;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

import engine.CurrentInput;
import engine.KeyboardMap;
import engine.Parameters;
import engine.Resources;
import engine.Window;
import util.*;
import world.Camera;
import world.World;
import world.parsers.WorldFactory;

public class PlayState extends BasicGameState {

	//*****PLAYER*****
	private Player player; // Bit, our lonely protagonist
	private Inventory inventory = new Inventory(); // Should be moved somewhere better eventually
	
	//*****GAME******
	private enum gameState {RUNNING, PAUSED, CUT_SCENE}
	private gameState currentState = gameState.RUNNING; // Indicates the current state of the game, whether it is 0.Playing, 1.Paused or 2. Cut-Scene
	private boolean inventoryOpen = false; // Indicates whether the inventory is open or not
	private DialogueText speechBubble; // The current speech bubble to display

	//*****WORLD*****
	private World world = new World(); // Current map
	private float renderScale = (float) Parameters.SIZE / Parameters.ORIGINAL_SIZE; // Move this somewhere
	private Camera camera;

	//*****RENDER*****
	private ArrayList<Renderable> renderQueue = new ArrayList<Renderable>();
	private Image alphaMap;
	private Graphics alphaMapGraphics;
	private static final boolean LIGHTING_ENABLED = Parameters.LIGHTING_ENABLED; // Will be changed to a variable later once it is possible to change between the two settings between maps. Mostly for debugging.

	//*****OTHER*****
	private CurrentInput input;
	private KeyboardMap debugMap;
	private boolean showDebug = false;
	private boolean slowMotion = false; // Every update will use 1 ms delta
	private boolean timed = false; // True if the current iteration of the gameloop is to be timed or not.
	private long opStart;

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {

		try {
			WorldFactory worldParserFactory = new WorldFactory();
			world = worldParserFactory.load(Parameters.INITIAL_MAP); //Load the initial map
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Could not load world! Quitting game...");
			System.exit(0);
		}

		debugMap = new KeyboardMap();
		input = new CurrentInput(debugMap, gc.getInput());
		
		// player = new Player(640, 534, world, input); // NOSONAR
		player = new Player(1245, 2294, world, input); // NOSONAR
		player.world = world;
		world.instances.add(player);

		camera = new Camera(player, world);
		camera.xPos = player.x - Window.getWidth() / 2;
		camera.yPos = player.y - Window.getHeight() / 2;
		player.direction = false;

		if (LIGHTING_ENABLED) {
			alphaMap = new Image(Window.getWidth(), Window.getHeight());
			alphaMapGraphics = alphaMap.getGraphics();
		}

		//DEBUGGING CODE BLOCKS BELOW//

		if (world.areaName.equals("Slope Test")) {
			showDebug = true;			
			world.debugDrawMode = true;
		}
		
		//world.instances.add(new FollowingBubble(200, 100, 500, 200, world, player));

		//world.instances.add(new MenuTerminal(input)); // NOSONAR
		
		// world.instances.add(new Logo(100,100,0,0));  // NOSONAR

		//String[] text = {"%NORMAL% This is a test...",
		//				"%NORMAL%%PAUSE 5000%A%PAUSE 5000%l%PAUSE 5000%l%PAUSE 5000%o",
		//				"%NORMAL% This is another test that should be on another line %PAUSE 1000%. %PAUSE 1000%. %PAUSE 1000%."}; // NOSONAR
						/*{"% NORMAL % Hello, young one!", //NOSONAR
						"% STATIC % My name is Gromit %PAUSE 1000%. %PAUSE 1000%. %PAUSE 1000%. %PAUSE 1000%",
						"% NORMAL % But you can %STATIC% call %NORMAL% me Baboo.",
						"I am a unique being from the land of % PAUSE   5000    % Nostrana. % NORMAL         %",
						"I eat food when I am hungry!"};*/ //NOSONAR
		//speechBubble = new SpeechBubble(text, 100, 600);

		/*for(int i = 0; i < 100; i++) {  // NOSONAR
			Mole newMole = new Mole(200 + i,300, world);
			enemies.add(newMole);
		}*/

		Mole newMole = new Mole(2500,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2600,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2700,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2800,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2900,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(3000,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2400,2200, world);
		world.instances.add(newMole);
		
		newMole = new Mole(2300,2200, world);
		world.instances.add(newMole);
		
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {

		if (timed) opStart = System.nanoTime(); // DEBUG

		g.setBackground(Color.gray); // Clear the previous frame
		
		world.render(g, camera); // Render the world and all its instances

		// Lighting
		if (LIGHTING_ENABLED) { // Render all the lights if they are enabled, this will eventually be moved to the world rendering
			alphaMapGraphics.setColor(Color.black);
			alphaMapGraphics.fillRect(0, 0, Window.getWidth(), Window.getHeight());
			alphaMapGraphics.setDrawMode(Graphics.MODE_ALPHA_MAP);
			alphaMapGraphics.setColor(new Color(0,0,0,0));

			for (int i=0; i < world.lights.size(); i++) {
				world.lights.get(i).render(alphaMapGraphics, camera.xPosAdj, camera.yPosAdj);
			}

			g.drawImage(alphaMap, 0, 0);
		}

		if (inventoryOpen) {
			inventory.render(g);
		}

		while (!renderQueue.isEmpty()) {
			Renderable renderable = renderQueue.remove(0);
			g.drawImage(renderable.image, renderable.x, renderable.y);
		}

		if (speechBubble != null) { // This will be probably moved to instances at some point, no reason they should be separate
			speechBubble.render(g);
		}

		if (slowMotion) {
			g.setColor(Color.white);
			g.drawString("SLOW", 24, Window.getHeight() - 34);
		}

		// Debug info
		if (showDebug) {
			g.setColor(Color.white);
			g.drawString("FPS: " + gc.getFPS() + " | Instances: " + world.instances.size() + " | Extra Solids: " + world.extraSolids.size(), 24, 24);
			g.drawString(String.format("X: %1$.1f | Y: %2$.1f | H: %3$.1f | V: %4$.1f | " + player.previousX + " | " + player.previousY, player.x, player.y, player.h_speed, player.v_speed), 24, 36);
			g.drawString("Action: " + player.currentAction.toString() + " | Jump: " + player.jumpReady,24, 48);
			
			String title = "Bits & Stuff";
			String names = "V. Gascon & A. Dion";
			String version = Parameters.GAME_VERSION;
			String releaseDate = "March 2018";

			Resources.LucidaBlackletter.drawString(Window.getWidth() - (Resources.LucidaBlackletter.getWidth(title)) - 24, Window.getHeight() - Resources.LucidaBlackletter.getHeight(title) - 70, title);
			g.drawString(names, Window.getWidth() - (g.getFont().getWidth(names)) - 24, Window.getHeight() - g.getFont().getHeight(names) - 52);
			g.drawString(version, Window.getWidth() - (g.getFont().getWidth(version)) - 24, Window.getHeight() - g.getFont().getHeight(version) - 32);
			g.drawString(releaseDate, Window.getWidth() - (g.getFont().getWidth(releaseDate)) - 24, Window.getHeight() - g.getFont().getHeight(releaseDate) - 16);
		}

		if (timed) {
			System.out.println("Rendered in " + String.format("%1$.1f",(System.nanoTime() - opStart) / 1000000.0) + "ms");
			timed = false;
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {

		if (slowMotion) {
			delta = 1; // In slow motion mode, the game calculates updates with a delta time of 1 ms. // NOSONAR, DEBUG
		}

		//*****INPUT*****// The following inputs are "immune" to the pause state
		if (gc.getInput().isKeyPressed(Input.KEY_T)) {  //DEBUG
			opStart = System.nanoTime();
			timed = true;
		}
		
		if (gc.getInput().isKeyPressed(debugMap.toggleSlowmotion)) {
			slowMotion = !slowMotion;
		}

		if (gc.getInput().isKeyPressed(debugMap.toggleFullscreen)) { // TEMP
			Window.setFullscreen(!Window.isFullscreen());
		}

		if (gc.getInput().isKeyPressed(Input.KEY_MINUS)) {
			if (Window.getDisplayModes().length - 1 > Window.getCurrentModeNumber()) {
				Window.setDisplayMode(Window.getCurrentModeNumber() + 1);
			}
		} else if ((gc.getInput().isKeyPressed(Input.KEY_EQUALS)) || (gc.getInput().isKeyPressed(Input.KEY_ADD))) {
			if (0 < Window.getCurrentModeNumber()) {
				Window.setDisplayMode(Window.getCurrentModeNumber() - 1);
			}
		}

		if (gc.getInput().isKeyPressed(debugMap.toggleDebug)) { //Show debug information
			showDebug = !showDebug;
			world.debugDrawMode = showDebug;
			gc.setShowFPS(!gc.isShowingFPS());
		}

		if (gc.getInput().isKeyPressed(debugMap.openInventory)) {
			inventoryOpen = !inventoryOpen;

			if (inventoryOpen) {
				inventory.open();
				inventory.addItem(new Item()); // DEBUG
			}

			if (currentState == gameState.PAUSED) {
				currentState = gameState.RUNNING;
			} else {
				currentState = gameState.PAUSED;
			}
		}

		if (currentState == gameState.RUNNING) { // Nothing will be updated if the game is paused. Everything should theoretically stay still

			//*****GAME INPUT*****
			if (gc.getInput().isMouseButtonDown(Input.MOUSE_RIGHT_BUTTON)) { // DEBUG
				player.x = gc.getInput().getMouseX() + camera.xPos;
				player.y = gc.getInput().getMouseY() + camera.yPos;
				player.v_speed = 0;
			}
			
			if (gc.getInput().isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) { // DEBUG
				world.instances.add(new ExperiencePoint(gc.getInput().getMouseX() + camera.xPos, gc.getInput().getMouseY() + camera.yPos, 5, world));
			}

			input.update();
			
			world.update(delta); // Takes care of updating all instances
			
			if (speechBubble != null) speechBubble.update(delta);

			camera.update(delta);

			//Update the lights for the pulsing animation
			if (LIGHTING_ENABLED) {
				for (int i=0; i < world.lights.size(); i++) {
					world.lights.get(i).update();
				}
			}

		} else if (currentState == gameState.PAUSED) {

			if (inventoryOpen) {
				inventory.update(delta);

				for (int i = 0; i < inventory.getSize(); i++) {

					if (inventory.getItem(i).checkHover(gc.getInput().getMouseX(), gc.getInput().getMouseY())) {
						renderQueue.add(new Renderable(gc.getInput().getMouseX(), gc.getInput().getMouseY(), inventory.getItem(i).infoPanel));
					}
				}
			} else {
				// Comming soon to a theater near you!
			}
		}

		if (timed) {
			System.out.println("Updated in " + String.format("%1$.1f",(System.nanoTime() - opStart) / 1000000.0) + "ms");
		}
	}

	@Override
	public int getID() {
		return States.PLAY;
	}
}
