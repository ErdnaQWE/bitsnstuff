package states;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;

public class GameIntroState extends BasicGameState {
	
	int step = 0; //Current step of the introduction
	boolean flash = false; //If the console line is on the screen or not
	int x_position = 0; //Position of the cursor in the console
	int y_position = 0;
	int y_view = 0;
	int y_print = 0;
	int accumulator = 0;
	int accumulator_flash = 0;
	
	ArrayList<String> console_text = new ArrayList<String>();
	

	@Override
	public void init(GameContainer gc, StateBasedGame s) throws SlickException {
		BufferedReader input;
		String str;
		try {
			input = new BufferedReader (new FileReader ("res/Comp"));			
			while((str=input.readLine())!=null)
				console_text.add(str);
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("FILE CANNOT BE FOUND, ALL IS LOST! BUT WHY!?!?!?!?.......");
		}
	}

	@Override
	public void render(GameContainer gc, StateBasedGame s, Graphics g) throws SlickException {
		if (flash) { //Draw the cursor
			g.fillRect(x_position * 9 + 4, y_position * 14  + 6, 2, 14);
		}
		for (int i = y_view; i < y_print; i++)
			g.drawString(console_text.get(i), 4, (i - y_view)*14 + 4);
	}

	@Override
	public void update(GameContainer gc, StateBasedGame s, int delta) throws SlickException {
		accumulator_flash += delta;
		accumulator += delta;
		
		System.out.println(delta);
		
		while (accumulator_flash >= 500) { //Make the cursor flash
			accumulator_flash -= 500;
			flash = !flash;
		}
		
		if (gc.getInput().isKeyPressed(Input.KEY_LEFT)) {
			if (x_position > 0)
				x_position -= 1;
		}
		else if (gc.getInput().isKeyPressed(Input.KEY_RIGHT)) {
			x_position += 1;
		}
		if (gc.getInput().isKeyPressed(Input.KEY_UP)) {
			if (y_position > 0)
				y_position -= 1;
		}
		else if (gc.getInput().isKeyPressed(Input.KEY_DOWN)) {
			y_position += 1;
		}
		
		if (accumulator >= 0 && y_print < console_text.size()) {
			if (y_print <= 3)
				accumulator -= 100;
			else if (y_print == 4)
				accumulator -= 5000;
			else if (y_print == 7)
				accumulator -= 2000;
			else if (y_print == 9)
				accumulator -= 1500;
			else if (y_print == 11)
				accumulator -= 500;
			else if (y_print == 13)
				accumulator -= 1000;
			else if (y_print == 17)
				accumulator -= 2000;
			else if (y_print == 22)
				accumulator -= 1000;
			else if (y_print == 47)
				accumulator -= 2000;
			else if (y_print == 49)
				accumulator -= 1000;
			else if (y_print <=54 && y_print > 49)
				accumulator -= 1000;
				
			y_print += 1;
			y_position = y_print;
			x_position =0;
		}
		
		
		
	}

	@Override
	public int getID() {
		return States.GAME_INTRO;
	}

}
