package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.attacks.Swing;
import instances.player.IAction;
import instances.player.Player;

public class Swinging implements IAction {

	private static SpriteSheet bitSwing = Resources.getSprite("bit_swing");

	public enum State {CHARCHING, ATTACKING, COMPLETED}
	State currentState = State.CHARCHING;
	
	private static final float CHARGE_TIME = 315; // Time needed for bit to prepare for an attack (such as a charging or casting time), probably gonna change to a variable depending on how I work out the other parts. 
	private float chargeTimeCur = CHARGE_TIME;
	private static final float ATTACK_TIME = 500;
	private float attackTimeCur = ATTACK_TIME;
	
	public static final float FRICTION = 150.0f; // The amount of friction from the ground for slides
	
	private final Player associatedPlayer;
	
	private int currentFrame = 0;
	
	public Swinging(Player player) {
		this.associatedPlayer = player;
	}

	@Override
	public void update(int delta) {
		
		associatedPlayer.applyGravity(delta);
		
		if (currentState == State.CHARCHING) {
			chargeTimeCur -= delta;
			
			currentFrame = Math.round(7 - (chargeTimeCur / (CHARGE_TIME / 7f)));  // Animation
		
			if (chargeTimeCur <= 0) {
				currentState = State.ATTACKING;
				
				if (associatedPlayer.direction) {
					associatedPlayer.h_speed = -150;
					associatedPlayer.world.instances.add(new Swing(associatedPlayer.x - 110, associatedPlayer.y, 50, 25, 5, associatedPlayer.world));
				}
				else {
					associatedPlayer.h_speed = 150;
					associatedPlayer.world.instances.add(new Swing(associatedPlayer.x + 100, associatedPlayer.y, 50, 25, 5, associatedPlayer.world));
				}
			}
		}
		
		if (currentState == State.ATTACKING) {
			attackTimeCur -= delta;
			
			currentFrame = Math.round(18 - (attackTimeCur / (ATTACK_TIME / 11f)));

			if (attackTimeCur <= 200) { // So the stomp of the swing stops the movement. Time to frame is approximate.
				associatedPlayer.h_speed = 0;
			}
						
			if (attackTimeCur <= 0) {
				currentState = State.COMPLETED;
			}
		}
		
		//Friction // Probably move this somewhere better, I have a feeling this is going to be used often
		float deltaAdj = delta / 1000.0f; //Adjusted delta (ms to s) for following calculations
		float newHSpeed;
		if (currentState != State.COMPLETED) {
			if (associatedPlayer.h_speed > 0) {
				newHSpeed = associatedPlayer.h_speed - FRICTION * deltaAdj;
				associatedPlayer.x += (associatedPlayer.h_speed + newHSpeed)/2 * deltaAdj;
				associatedPlayer.h_speed = newHSpeed;
			} else if (associatedPlayer.h_speed < 0) {
				newHSpeed = associatedPlayer.h_speed + FRICTION * deltaAdj;
				associatedPlayer.x += (associatedPlayer.h_speed + newHSpeed)/2 * deltaAdj;
				associatedPlayer.h_speed = newHSpeed;
			}	
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		// Does nothing for now, comboing will be added here at some point
	}
	
	@Override
	public void render(Graphics g, float x, float y) {
		bitSwing.getSubImage(currentFrame, 0).getFlippedCopy(associatedPlayer.direction, false).getScaledCopy(2).draw(x - 44, y - 30);
	}

	@Override
	public boolean isCompleted() {
		return currentState == State.COMPLETED;
	}

	@Override
	public boolean isAnimationCompleted() {
		return currentState == State.COMPLETED;
	}
}
