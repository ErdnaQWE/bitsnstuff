package instances.player.actions;

import org.newdawn.slick.Graphics;

import instances.player.Player;

public class RightRunning extends LeftRunning {	

	public RightRunning(Player player) {
		super(player);
	}

	@Override
	public void update(int delta) {		
		associatedPlayer.h_speed = WALK_SPEED;
		
		associatedPlayer.applyGravity(delta);
		
		frameTime += delta; 

		while (frameTime > BASE_TIME_PER_FRAME) { // TODO MAKE THIS BETTER WITHOUT A WHILE LOOP, LIKE MAKE A FORMULA, DON'T BE LAZY KIDS
			currentFrame += 1;
			frameTime -= BASE_TIME_PER_FRAME;
		}
		
		if (currentFrame >= 21) {
			currentFrame = 4;  // Don't reanimate the movement initiation
		}		
	}
	
	@Override
	public void render(Graphics g, float x, float y) {
		BIT_RUN.getSubImage(currentFrame, 0).getFlippedCopy(false, false).getScaledCopy(2).draw(x - 46, y - 28);
	}

}
