package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.Player;
import instances.player.Player.Weapon;

public class Idling implements IAction {

	private enum ANIMATION_STATE {NORMAL, SECONDARY}
	private ANIMATION_STATE currentState = ANIMATION_STATE.NORMAL;
	
	private int currentFrame = 0; // Unused for now, there should be an animation here sometime soon
	private int secondaryTimer = 0; //
	private int frameTime = 0;
	
	private final Player player;
	
	private SpriteSheet bitStand;
	private SpriteSheet bitSecondary;
	private int spriteXOffset = 0;
	private int spriteYOffset = 0;
	
	public Idling(Player player) {
		this.player = player;
		
		this.player.h_speed = 0;
		
		if (player.currentWeapon == Weapon.KNIVES) {
			bitStand = Resources.getSprite("bit_idle_knives");
			spriteXOffset = -43;
			spriteYOffset = -28;
		} else {
			bitStand = Resources.getSprite("bit_stand"); // Defaults to Fists (no weapon) sprites
			bitSecondary = Resources.getSprite("bit_stand_shrug");
			spriteXOffset = -45;
			spriteYOffset = -30;
		}
	}

	@Override
	public void update(int delta) {
		frameTime += delta;
		
		//player.applyGravity(delta);
		
		switch (player.currentWeapon) {
			case KNIVES:
				if (frameTime > 150) {
					frameTime -= 150;
					currentFrame = (currentFrame + 1) % 5;
				}
				break;

			case FISTS:
				if (currentState == ANIMATION_STATE.NORMAL) {
					if (frameTime > 125) {
						frameTime -= 125;
						
						if (currentFrame == 6) {
							currentFrame = 0;
							secondaryTimer += 1;
						} else {
							currentFrame += 1;
						}
					}
					
					if (secondaryTimer >= 3) {
						secondaryTimer = 0;
						currentState = ANIMATION_STATE.SECONDARY;
						frameTime = 0;
						currentFrame = 0;
					}
				}
				else if (currentState == ANIMATION_STATE.SECONDARY){
					if (frameTime > 125) {
						frameTime -= 125;
						currentFrame += 1;
						
						if (currentFrame == 15) {
							currentFrame = 0;
							frameTime = 0;
							currentState = ANIMATION_STATE.NORMAL;
						}
					}
				}
				break;
				
			case SWORD:
			default:
				currentFrame = 0;
				frameTime = 0;
				break;
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		// Does nothing
	}
	
	@Override
	public void render(Graphics g, float x, float y) {
		if (currentState == ANIMATION_STATE.NORMAL) {
			bitStand.getSubImage(currentFrame, 0).getFlippedCopy(player.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset);
		}
		else if (currentState == ANIMATION_STATE.SECONDARY) {
			bitSecondary.getSubImage(currentFrame, 0).getFlippedCopy(player.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset + 6);
		}
	}

	@Override
	public boolean isCompleted() {
		return true;  // Always ready to exit idling
	}

	@Override
	public boolean isAnimationCompleted() {
		return true;  // Always ready to exit idling
	}
}
