package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.Player;

public class Jumping implements IAction {

	private static SpriteSheet bitJump = Resources.getSprite("bit_jump");
	
	private enum State {PRE, INTRA, POST, DONE}
	private State currentState = State.INTRA;
	
	private static final float AMORTIZATION_TIME = 125;
	private float amortizationTimeCur = AMORTIZATION_TIME;
	
	private boolean leftWasPressed = false;
	private boolean rightWasPressed = false;
	
	private int spriteXOffset = -21;
	private int spriteYOffset = -34;
	
	private final Player associatedPlayer;

	public Jumping(Player player, boolean triggerJump) {
		
		this.associatedPlayer = player;
		
		if (triggerJump) {
			associatedPlayer.h_speed = 0; // Stop horizontal movement ? Maybe remove this
			associatedPlayer.v_speed = -300;
			associatedPlayer.jumpReady = false;
		}
	}

	@Override
	public void update(int delta) {		
		switch (currentState) {
			case INTRA:
				if (associatedPlayer.v_speed == 0 && associatedPlayer.jumpReady) {
					currentState = State.POST;
				}
				
				break;
				
			case POST:
				amortizationTimeCur -= delta;
				
				if (amortizationTimeCur <= 0) {
					currentState = State.DONE;
					amortizationTimeCur = 0;
				}
				
				break;
				
			default:  // Don't know why I need this, keeping it just to be safe
				break;
		}
		
		associatedPlayer.applyGravity(delta); // Make the 0 v_speed checks before applying the gravity again (meaning that it would never be 0)
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		if (input.leftMoveButton) {
			associatedPlayer.h_speed = -100;
			leftWasPressed = true;
			associatedPlayer.direction =true;
		} else if (leftWasPressed) {
			associatedPlayer.h_speed = 0;
			leftWasPressed = false;
		}
		
		if (input.rightMoveButton) {
			associatedPlayer.h_speed = 100;
			rightWasPressed = true;
			associatedPlayer.direction =false;
		} else if (rightWasPressed) {
			associatedPlayer.h_speed = 0;
			rightWasPressed = false;
		}
	}
	
	@Override
	public void render(Graphics g, float x, float y) {
		switch (currentState) {
		
			case INTRA:
				bitJump.getSubImage((int) Math.floor(Math.min((associatedPlayer.v_speed + 300), 600) / (600f / 19f)), 0).getFlippedCopy(associatedPlayer.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset);
				break;
				
			case POST:
			case DONE:
				bitJump.getSubImage(Math.round(28 - (amortizationTimeCur / (AMORTIZATION_TIME / 7f))), 0).getFlippedCopy(associatedPlayer.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset);
				break;
				
			default:
				break;
		}
	}

	@Override
	public boolean isCompleted() {
		return currentState == State.POST || currentState == State.DONE;
	}

	@Override
	public boolean isAnimationCompleted() {
		return currentState == State.DONE;
	}
}
