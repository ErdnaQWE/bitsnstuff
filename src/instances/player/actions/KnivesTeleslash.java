package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.Player;

public class KnivesTeleslash implements IAction {

	private enum ANIMATION_STATE {PRE, POST, DONE}
	private ANIMATION_STATE currentState = ANIMATION_STATE.PRE;
	
	private int currentFrame = 0;
	private int frameTime = 0;
	
	private final Player player;
	
	private SpriteSheet bitTeleslash;
	private int spriteXOffset = -105;
	private int spriteYOffset = -28;
	
	public KnivesTeleslash(Player player) {
		this.player = player;
		
		// Small initial speed boost
		if (player.direction) {
			this.player.h_speed = -125;
		} else {
			this.player.h_speed = 125;
		}
		
		this.bitTeleslash = Resources.getSprite("bit_teleslash");
	}
	
	@Override
	public void update(int delta) {
		frameTime += delta;
		
		if (frameTime > 40) {
			currentFrame = (currentFrame + 1);
			frameTime -= 40;
		}
		
		if (currentFrame > 16) {
			currentState = ANIMATION_STATE.DONE;
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		// NOTHING
	}

	@Override
	public void render(Graphics g, float x, float y) {
		bitTeleslash.getSubImage(currentFrame, 0).getFlippedCopy(player.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset);
	}

	@Override
	public boolean isCompleted() {
		return isAnimationCompleted();
	}

	@Override
	public boolean isAnimationCompleted() {
		return currentState == ANIMATION_STATE.DONE;
	}
}
