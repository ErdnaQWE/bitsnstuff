package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.Player;

public class KnivesSlash implements IAction {

	private enum ANIMATION_STATE {PRE, POST, DONE}
	private ANIMATION_STATE currentState = ANIMATION_STATE.PRE;
	
	private int currentFrame = 0;
	private int frameTime = 0;
	
	private final Player player;
	
	private boolean directionWasPressed = true;
	
	private SpriteSheet bitKnivesSlash;
	private int spriteXOffset = -105;
	private int spriteYOffset = -28;
	
	public KnivesSlash(Player player) {
		this.player = player;
		
		this.player.h_speed = 0;
		
		this.bitKnivesSlash = Resources.getSprite("bit_teleslash");
	}
	
	@Override
	public void update(int delta) {
		frameTime += delta;
		
		player.applyGravity(delta);
		
		if (frameTime > 100) {
			currentFrame = (currentFrame + 1);
			frameTime -= 100;
			
			if (currentFrame > 2) {
				currentState = ANIMATION_STATE.DONE;
			}
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		
		if (player.direction) { // Attacking left
			if (directionWasPressed && !input.leftActionButton) {
				directionWasPressed = false;
			} else if (!directionWasPressed && input.leftActionButton){
				player.currentAction = new KnivesTeleslash(player);
			}
		} else {
			if (directionWasPressed && !input.rightActionButton) {
				directionWasPressed = false;
			} else if (!directionWasPressed && input.rightActionButton){
				player.currentAction = new KnivesTeleslash(player);
			}
		}
	}

	@Override
	public void render(Graphics g, float x, float y) {
		bitKnivesSlash.getSubImage(1, 0).getFlippedCopy(player.direction, false).getScaledCopy(2).draw(x + spriteXOffset, y + spriteYOffset);
	}

	@Override
	public boolean isCompleted() {
		return isAnimationCompleted();
	}

	@Override
	public boolean isAnimationCompleted() {
		return currentState == ANIMATION_STATE.DONE;
	}
}
