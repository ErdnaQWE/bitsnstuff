package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.LedgeGraber;
import instances.player.Player;

public class LedgeGrab implements IAction {

	private final Player associatedPlayer;
	private int frameTime;
	private int currentFrame;
	private static final int TIME_PER_FRAME = 20;
	private LedgeGraber grabbedBy; // The ledge grabber hitbox that caught the block, will receive a delay before next grab (to avoid glitches where Bit automatically grabs the block again) // TODO lel
	
	private static final int X_OFFSET = -21;
	private static final int Y_OFFSET = -26;
	
	protected final SpriteSheet BIT_GRAB;
	
	/**
	 * The state where Bit holds on to a ledge.
	 * During this state, Bit should not move. 
	 * The only way to escape this action is to jump or perform an action in the oposite direction.
	 * 
	 * @param The player that is currently in this state
	 * @param The LedgeGraber instance that triggered the grab. Will receive a cooldown on action exit.
	 */
	public LedgeGrab(Player player, LedgeGraber grabbedBy) {
		
		this.associatedPlayer = player;
		this.frameTime = 0;
		this.grabbedBy = grabbedBy;
		
		this.BIT_GRAB = Resources.getSprite("bit_ledge");
	}
	
	@Override
	public void update(int delta) {
		frameTime += delta;
		
		associatedPlayer.v_speed = 0; // This will be removed once the actions handle the gravity
		
		// TODO MAKE THIS BETTER WITHOUT A WHILE LOOP, LIKE MAKE A FORMULA, DON'T BE LAZY KIDSs
		while (frameTime > TIME_PER_FRAME && currentFrame < 8) {
			currentFrame += 1;
			frameTime -= TIME_PER_FRAME;
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		// This is is the only way to escape from this function, Bit is stuck until the user presses a button
		
		if (input.jumpButton) {
			associatedPlayer.currentAction = new Jumping(associatedPlayer, true);
		} else if (associatedPlayer.direction && input.rightMoveButton) {
			associatedPlayer.currentAction = new RightRunning(associatedPlayer);
			associatedPlayer.direction = associatedPlayer.DIRECTION_RIGHT;
		} else if (!associatedPlayer.direction && input.leftMoveButton) {
			associatedPlayer.currentAction = new LeftRunning(associatedPlayer);
			associatedPlayer.direction = associatedPlayer.DIRECTION_LEFT;
		}
	}

	@Override
	public void render(Graphics g, float x, float y) {
		BIT_GRAB.getSubImage(currentFrame, 0)
				.getFlippedCopy(associatedPlayer.direction, false)
				.getScaledCopy(2)
				.draw(x + X_OFFSET, y + Y_OFFSET);
	}

	@Override
	public boolean isCompleted() {
		return false; // Never completed, change action in input update
	}

	@Override
	public boolean isAnimationCompleted() {
		return false; // Never completed, change action in input update
	}

}
