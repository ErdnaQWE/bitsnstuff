package instances.player.actions;

import org.newdawn.slick.Graphics;

import engine.CurrentInput;
import instances.player.IAction;

public class Range implements IAction {
	
	private enum State {CHARGING, RELEASED, DONE}
	private State currentState = State.CHARGING;

	@Override
	public void update(int delta) {
		// Animation update (arc and segment positions)
		switch (currentState) {
		case CHARGING:
			break;
			
		case DONE:
			break;
			
		case RELEASED:
			break;
			
		default:
			break;
		}
	}

	@Override
	public void updateFromInput(CurrentInput input) {
		// Detect release of action key and change state
	}

	@Override
	public void render(Graphics g, float x, float y) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isCompleted() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isAnimationCompleted() {
		// TODO Auto-generated method stub
		return false;
	}


}
