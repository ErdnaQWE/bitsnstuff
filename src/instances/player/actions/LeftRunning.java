package instances.player.actions;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.CurrentInput;
import engine.Resources;
import instances.player.IAction;
import instances.player.Player;
import instances.player.Player.Weapon;

public class LeftRunning implements IAction {

	//private final SpriteSheet BIT_WALK = Resources.getSprite("bit_walk"); // Old animation
	protected final SpriteSheet BIT_RUN;  // Flip all of the images in the constructor? Or at loading time? 
	
	protected final Player associatedPlayer;
	
	protected int currentFrame = 0;
	protected int frameTime = 0;
	protected final float BASE_TIME_PER_FRAME = 40;  // In milliseconds
	
	final float WALK_SPEED = 100.0f; //Bit walking speed for debugging purposes. Will be affected by stats at some point later on
	
	public LeftRunning(Player player) {
		this.associatedPlayer = player;  // TODO: Calculate the BASE_TIME_PER_FRAME here according to the player speed
		
		if (associatedPlayer.currentWeapon == Weapon.KNIVES) {
			BIT_RUN = Resources.getSprite("bit_run_knives");
		} else {
			BIT_RUN = Resources.getSprite("bit_run");
		}
	}

	@Override
	public void update(int delta) {
		associatedPlayer.h_speed = -WALK_SPEED;
		
		associatedPlayer.applyGravity(delta);
				
		frameTime += delta; 

		while (frameTime > BASE_TIME_PER_FRAME) { // TODO MAKE THIS BETTER WITHOUT A WHILE LOOP, LIKE MAKE A FORMULA, DON'T BE LAZY KIDS
			currentFrame += 1;
			frameTime -= BASE_TIME_PER_FRAME;
		}
		
		if (currentFrame >= 21) {
			currentFrame = 4;  // Don't reanimate the movement initiation
		}
	}
	
	@Override
	public void updateFromInput(CurrentInput input) {
		// Does nothing
	}

	@Override
	public void render(Graphics g, float x, float y) {
		BIT_RUN.getSubImage(currentFrame,0).getFlippedCopy(true, false).getScaledCopy(2).draw(x - 43, y - 28);
	}

	@Override
	public boolean isCompleted() {
		return true;  // In this case, Action is always completed
	}

	@Override
	public boolean isAnimationCompleted() {
		return true;
	}
}
