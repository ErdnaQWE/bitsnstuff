/* LEDGE GRABER (patent pending)
 * 
 * A small hitbox type of thing that will check for contacts with ledges so Bit can grab onto them or not
 */
package instances.player;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import instances.Instance;
import instances.player.actions.LedgeGrab;
import util.Slope;
import util.Solid;

public class LedgeGraber extends Instance {
	
	private Player associatedPlayer; // The player associated with the ledge grab
	private boolean IS_LEFT; // Whether or not this ledge graber is on the left of bit or not, otherwise, it will be on the right (Probably a better to way to handle this hehe)
		
	public LedgeGraber(Player player, boolean IS_LEFT) {
		super (0, 0, 1, 1, null);
		
		this.associatedPlayer = player;
		this.IS_LEFT = IS_LEFT;
	}
	
	@Override
	public void update(int delta) {
		if (IS_LEFT) {
			x = associatedPlayer.x - 5;
		} else {
			x = associatedPlayer.x + associatedPlayer.width + 5;
		}
		
		y = associatedPlayer.y + 26;
		
		if (!associatedPlayer.currentAction.getClass().equals(LedgeGrab.class)) {
			Solid foundSolid = associatedPlayer.world.getSolidAtPoint(x, y);
			
			if (foundSolid != null) { // Found the first solid, now make sure that nothing is over it
				if (foundSolid.getClass() != Slope.class) {
					foundSolid = associatedPlayer.world.getSolidAtPoint(x, y - 5); // All it takes is two pixels of free air (but the engine won't really allow less than a full tile of free space
					
					if (foundSolid == null) {
						associatedPlayer.currentAction = new LedgeGrab(associatedPlayer, this);
					}
				}
			}
		}
	}
	
	@Override
	public void render(Graphics g, float offsetX, float offsetY) {
		// Nothing, invisible hitboxes
		
		g.setColor(Color.red);
		g.fillRect(x + offsetX, y + offsetY, width, height);
	}
	
	@Override
	public boolean isRemovable() {
		return false;
	}
}