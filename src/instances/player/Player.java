package instances.player;

import org.newdawn.slick.Graphics;

import engine.CurrentInput;
import instances.Damage;
import instances.Instance;
import instances.player.actions.*;
import world.World;

public class Player extends Instance {
		
	//*****STATISTICS*****
	public enum Weapon {KNIVES, BOW, FISTS, SWORD} // Each weapon will eventually be changed to an instance, or the system will probably be completely different
	public Weapon currentWeapon = Weapon.FISTS;
	
	public IAction currentAction = new Idling(this); // The current Action Bit is doing
	
	public CurrentInput handlingInput;
		
	public Player(float x, float y, World world, CurrentInput input) {
		super(x,y,38,74, world);
		
		tags.add(INSTANCE_TAG.PLAYER);
		tags.add(INSTANCE_TAG.DAMAGEABLE);
		tags.add(INSTANCE_TAG.CONTACTABLE);
		tags.add(INSTANCE_TAG.TANGIBLE);
		
		world.instances.add(new LedgeGraber(this, true));
		world.instances.add(new LedgeGraber(this, false));
		
		this.handlingInput = input;
	}
	
	@Override
	public void update(int delta) {
		// previousX = x;
		// previousY = y;
		
		currentAction.updateFromInput(handlingInput);

		// Check for possible action changes (the order is important)
		checkJump();
		checkMove();
		checkAttack();
		
		currentAction.update(delta);

		super.update(delta); // Previous x and y
				
		x += h_speed * delta / 1000.0f;
	}
	
	@Override
	public void render(Graphics g, float offsetx, float offsety) {
		currentAction.render(g, x + offsetx, y + offsety); // There should always be an action to render, or else something went wrong somewhere
		//super.render(g, offsetx, offsety); // Show hitbox
	}

	@Override
	public void render(Graphics g) { // Render the player with no offset, mostly unused
		render(g,0,0);
	}

	
//*****MOVEMENT/INPUT CODE*****
	
	/**
	 * Should be called from the engine when the player wants to attack. 
	 * Will handle all the logic behind holding the attack button. Some weapons and attacks react differently to the player holding the button
	 */
	private void checkAttack() {
		
		if (currentAction.isCompleted()) {
			if (handlingInput.downActionButton) { 
				currentAction = new Swinging(this);
			}
			else if (handlingInput.leftActionButton) {
				direction = true;
				currentAction = new KnivesSlash(this);
			}
			else if (handlingInput.rightActionButton) {
				direction = false;
				currentAction = new KnivesSlash(this);
			}
			else if (handlingInput.upActionButton) {
				// No action exists yet
			}
		}
	}
	
	/** Should be called from the engine when the player wants to jump
	 * Will handle the logic behind jumping. This works because all instances are aware of the world they are in.
	 */
	private void checkJump() {
		if (currentAction.isCompleted()) {
			if (handlingInput.jumpButton && jumpReady) {
				currentAction = new Jumping(this, true);
				jumpReady = false;
			} else if (v_speed != 0 && currentAction.getClass().equals(Idling.class)) {
				currentAction = new Jumping(this, false);
			}
		}
	}
	
	private void checkMove() {
		
		if (currentAction.isCompleted()) {  // Can move if and only if there is no current action
			if (handlingInput.leftMoveButton) {
				if (!(currentAction.getClass().equals(LeftRunning.class))) {
					currentAction = new LeftRunning(this);
					direction = true;
				}
			} else if (handlingInput.rightMoveButton) {
				if (!(currentAction.getClass().equals(RightRunning.class))) {
					currentAction = new RightRunning(this);
					direction = false;
				}
			} else  {
				// Nothing is pressed, can I switch to idle?
				if (!(currentAction instanceof Idling) && currentAction.isAnimationCompleted()) {
					currentAction = new Idling(this);
				}
			}
		}
	}
	
	// Unused for now as far as I know, all the logic here should be moved in checkMove() at some point
	public void releaseMove(boolean upReleased, boolean downReleased, boolean leftReleased, boolean rightReleased) {
		if (currentAction.isCompleted() && currentAction.isAnimationCompleted()) {
			currentAction = new Idling(this);
		}
	}
	
	@Override
	public void contact(Instance instance) {
		// Nothing for now, probably wil stay nothing for a while
	}
	
	@Override
	public void inflictDamage(Damage damage) {
		// Some calculations here, probably gonna be handed over to the stats class
	}
}
