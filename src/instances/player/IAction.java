package instances.player;

import org.newdawn.slick.Graphics;

import engine.CurrentInput;

// An ability used by Bit, mostly attacks

public interface IAction {
	
	// Frame updates
	void update(int delta);
	
	// Updates triggered by input
	void updateFromInput(CurrentInput input);
	
	void render(Graphics g, float x, float y);
	
	// Returning true for this means that the action is ready to be replaced by another one
	boolean isCompleted();

	// Returning true for this means that this action can be replaced by the Idling action (for animation purposes)
	boolean isAnimationCompleted();
	
}
