package instances;

public class Damage {
	// Some damage that will be dealt to an enemy, this will contain all sorts of information and functions allowing for example to exclude or include certain types of damage in calculations
	
	public float STANDARD_DAMAGE = 0;
	
	public Damage(float amount) {
		this.STANDARD_DAMAGE = amount;
	}
}
