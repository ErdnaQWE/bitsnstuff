package instances.particles;

import org.newdawn.slick.Color;

import world.World;

public class SnowSource extends ParticleSource {

	public SnowSource(float x, float y, World world) {
		super(x, y, world);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void update(int delta) {
		
		for (int i = 0; i < 10; i++) {
			particles.addLast(new Particle(x, y, (float) Math.random() * 1000 - 500, (float) Math.random() * 1000 - 500, (float) Math.random() * 5, Float.MAX_VALUE, Color.white));
		}
		
		super.update(delta);
	}
	

}
