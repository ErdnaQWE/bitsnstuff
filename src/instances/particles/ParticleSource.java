package instances.particles;

import java.util.Iterator;
import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import instances.Instance;
import world.World;

public class ParticleSource extends Instance {

	protected LinkedList<Particle> particles;
	protected static Iterator<Particle> iterator;
	
	private final float GRAVITY = 500.0f; // Gravity constant
		
	private float vSpeedNew;
	private float delta_adj;
	private static Particle currentParticle;
	private Color currentColor;
	
	public ParticleSource(float x, float y, World world) {
		super(x, y, 0, 0, world);
		
		particles = new LinkedList<Particle>();
		
	}
	
	public ParticleSource(float x, float y, Image image, World world) {
		super(x,y,0,0,world);
				
		particles = new LinkedList<Particle>();
		
		for (int xPos = 0; xPos < image.getWidth(); xPos++) {
			for (int yPos = 0; yPos < image.getHeight(); yPos++) {
				
				currentColor = image.getColor(xPos, yPos);
				
				if (currentColor != Color.transparent) {
					//for (int ii = 0; ii < 1; ii++) {
						particles.addLast(new Particle(x + xPos * 2, y + yPos * 2, (float) Math.random() * -300 - 100, (float) Math.random() * 300 - 150, (float) Math.random() * 8 , 4000, image.getColor(xPos, yPos)));
					//}
				}
			}
		}
		
	}
	
	@Override
	public void render (Graphics g, float offsetX, float offsetY) {
		
		iterator = particles.iterator();
		
		while (iterator.hasNext()) {
			
			currentParticle = iterator.next();
			
			g.setColor(currentParticle.color);
			g.fillRect(currentParticle.x + offsetX, currentParticle.y + offsetY, 2 + currentParticle.z, 2 + currentParticle.z);
			
		}
				
		g.setColor(Color.red);
		g.drawString(Integer.toString(particles.size()), x + offsetX, y + offsetY);
	
	}
	
	@Override
	public void update (int delta) {
		
		iterator = particles.iterator();
		delta_adj = delta / 1000.0f;
		
		while (iterator.hasNext()) {
			
			currentParticle = iterator.next();
			
			currentParticle.life -= delta;
			
			if (currentParticle.life > 0) {
			
				vSpeedNew = currentParticle.vVelocity + GRAVITY * delta_adj;
				currentParticle.y += (currentParticle.vVelocity + vSpeedNew)/2 * delta_adj;
				currentParticle.vVelocity = vSpeedNew;
				
				currentParticle.x += currentParticle.hVelocity * delta_adj;
				currentParticle.z += currentParticle.zVelocity * delta_adj;

			} else {
				iterator.remove();
			}
		}
	}
	
	public LinkedList<Particle> getParticles() {
		return particles;
	}
	
	@Override
	public boolean isRemovable() {
		return particles.isEmpty();
	}
}
