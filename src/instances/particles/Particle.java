package instances.particles;

import org.newdawn.slick.Color;

public class Particle {

	public float x;
	public float y;
	public float z;
	public float vVelocity;
	public float hVelocity;
	public float zVelocity;
	public Color color;
	public float life;
	
	public Particle (float x, float y, float vVelocity, float hVelocity, float zVelocity, float life, Color color) {
		this.x = x;
		this.y = y;
		this.z = 0;
		this.vVelocity = vVelocity;
		this.hVelocity = hVelocity;
		this.zVelocity = zVelocity;
		this.life = life;
		this.color = color;
	}
	
}
