package instances.particles;


import org.newdawn.slick.Color;

import engine.Parameters;
import world.World;

public class HeavyRainSource extends ParticleSource {

	private float delta_adj;
	private Particle currentParticle;
	
	//private LinkedList<Particle> particles;
		
	public HeavyRainSource(float x, float y, World world) {
		super(x, y, world);
		// TODO Auto-generated constructor stub		
	}
	
	@Override
	public void update(int delta) {
		
		while (particles.size() < 2000) {
			particles.add(new Particle((float) Math.random() * world.WIDTH * Parameters.SIZE, y + (float) Math.random() * 400 - 200, 600f, 600f, (float) Math.random() * 5, Float.MAX_VALUE, Color.blue));
		}
		
		iterator = particles.iterator();
		delta_adj = delta / 1000.0f;
		
		while (iterator.hasNext()) {
						
			currentParticle = iterator.next();
			
			currentParticle.life -= delta;
			
			if (currentParticle.life > 0) {
			
				//vSpeedNew = currentParticle.vVelocity + GRAVITY * delta_adj;
				//currentParticle.y += (currentParticle.vVelocity + vSpeedNew)/2 * delta_adj;
				//currentParticle.vVelocity = vSpeedNew;
				
				currentParticle.x += currentParticle.hVelocity * delta_adj * Math.sqrt(currentParticle.zVelocity) / 2;
				currentParticle.y += currentParticle.vVelocity * delta_adj * Math.sqrt(currentParticle.zVelocity) / 2;
				currentParticle.z += currentParticle.zVelocity * delta_adj;
				
				if (currentParticle.x > world.WIDTH * Parameters.SIZE || currentParticle.x < 0 || currentParticle.y > world.HEIGHT * Parameters.SIZE) {
					iterator.remove();
				}

			} else {
				iterator.remove();
			}
			
		}
		
	}

}
