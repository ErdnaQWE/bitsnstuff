package instances;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

import engine.Resources;

public class Logo extends Instance {
	
	final int BIT_SIZE = 1;
	
	Image one = Resources.getImage("1").getScaledCopy(BIT_SIZE);
	Image zero = Resources.getImage("0").getScaledCopy(BIT_SIZE);

	public Logo(float x, float y, float width, float height) {
		super(x, y, 0, 0, null);
		letters = new BitLetter('b');
	}
	
	BitLetter letters;
	
	public void update(int delta) {
		
		letters.randomize(2);
		
		return;
	}
	
	public void render(Graphics g, float offsetX, float offsetY) {
				
		g.setColor(Color.white);
		
		float renderX = x;
		float renderY = y;
		
		for (int ry = 0; ry < letters.bits[0].length; ry++) {
			for (int rx = 0; rx < letters.bits.length; rx++) {
				//System.out.println(rx + "|" + ry + "|" + letters.bits[rx][ry]);
				if (letters.bits[rx][ry] == 0) {
					//System.out.println("Rendered 0 " + rx + "|" + ry + renderX + "|" + renderY);
					g.drawImage(zero, renderX, renderY);
					renderX += zero.getWidth() + 1;
				} else if (letters.bits[rx][ry] == 1) {
					//System.out.println("Rendered 1 " + rx + "|" + ry + " at " + renderX + "|" + renderY);
					g.drawImage(one, renderX, renderY);
					renderX += one.getWidth() + 1;
				} else {
					renderX += zero.getWidth() + 1 ;
				}
			}
			renderX = x;
			renderY += zero.getHeight() + 1;
		}
		
	}
	
	class BitLetter {
		
		int bits[][];
		
		public BitLetter(char c) {
						
			Image letter = Resources.getImage("B");
			
			int width = letter.getWidth();
			int height = letter.getHeight();
			
			bits = new int[width][height];

			for(int x = 0; x < width; x += 1) {
				for (int y = 0; y < height; y += 1) {
					if (letter.getColor(x, y).equals(Color.black)) {
						if ((x + (y%2)) % 2 == 0) {
							bits[x][y] = 0;
						} else {
							bits[x][y] = 1;
						}
					} else {
						bits[x][y] = 2;
					}
					
					/*if (letter.getColor(x, y).equals(Color.black)) {
						bits[x*2][y*2] = 0;
						bits[x*2 + 1][y*2] = 1;
						bits[x*2][y*2 + 1] = 1;
						bits[x*2 + 1][y*2 + 1] = 0;
					} else {
						bits[x*2][y*2] = 2;
						bits[x*2 + 1][y*2] = 2;
						bits[x*2][y*2 + 1] = 2;
						bits[x*2 + 1][y*2 + 1] = 2;
					}*/
				}
			}
			
		}
		
		public void randomize(int amount) {
			
			int x;
			int y;
			
			while (amount > 0) {
				
				x = (int) (Math.random() * bits.length);
				y = (int) (Math.random() * bits[x].length);
				
				if (bits[x][y] != 2) {
					if (bits[x][y] == 0) {
						bits[x][y] = 1;
					} else {
						bits[x][y] = 0;
					}
					amount -= 1;
				}
			}
			
		}
		
		
		
		
		
	}
	
	
	
	

}
