package instances;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Circle;

/*	A point of light, such as a torch and such.
 * 
 */

public class Light {

	private float x;
	private float y;
	public float radius; //Radius of the light in pixels
	private float radiusDifference;
	public boolean pulsing; 
	
	public Instance associatedInstance;
	
	public Light(float x, float y, float radius, boolean pulsing, Instance associatedInstance) {
		this.x = x;
		this.y = y;
		this.radius = radius;
		this.pulsing = pulsing;
		this.associatedInstance = associatedInstance;
		
		radiusDifference = 0;
	}
	
	public Light(float x, float y, float radius, boolean pulsing) {
		this(x,y,radius,pulsing,null);
	}
	
	public Light(float x, float y) {
		this(x,y,10,true, null);
	}
	
	public Light(float x, float y, float radius) {
		this(x, y, radius, true, null);
	}

	public void update() {
		//TODO Delta based???
		if (radiusDifference > 40) { //If it's bigger than the max size...
			radiusDifference -= 0.5; //... then bring it closer.
		} else if (radiusDifference < -40) { //If it's small than min size...
			radiusDifference += 0.5; //... then bring it closer.
		} else {
			int randomNum = (int) Math.round(Math.random() * 100);
			if (randomNum == 0) { // About once every second or two...
				randomNum = (int) Math.round(Math.random() * 2);
				if (randomNum == 0) {
					radiusDifference += Math.random() * 12; //... big jumps.
				} else {
					radiusDifference -= Math.random() * 12;
				}
			} else { //Small fluctuations
				randomNum = (int) Math.round(Math.random() * 80) - 40;
				if (randomNum > radiusDifference) {
					radiusDifference += Math.random();
				} else {
					radiusDifference -= Math.random();
				}
			}
		}
	}
	
	public void render(Graphics g, float offsetx, float offsety) {
		g.fill(new Circle(getX() - offsetx, getY() - offsety, radius + radiusDifference));
	}
	
	public float getX() {
		if (associatedInstance == null) {
			return x;
		} else {
			return associatedInstance.getCenterX();
		}
	}
	
	public float getY() {
		if (associatedInstance == null) {
			return y;
		} else {
			return associatedInstance.getCenterY();
		}
	}
	
}
