package instances;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import engine.Parameters;
import util.Box;
import world.World;

public class Instance extends Box {
		
	private boolean renderHitBox = true;
	
	public float v_speed = 0; // Vertical speed (in pixels)
	public float h_speed = 0; // Horizontal speed (in pixels) // Probably unused, we will see
	
	public enum INSTANCE_TAG {	TANGIBLE,		// Indicates that the instance is currently affected by solid tiles. In other words it cannot travel through anything
								DAMAGEABLE,		// Indicates that the instance can currently receive damage, usually used in conjunction with the ENEMY and HOSTILE tags
								HOSTILE,		// Indicates that the instance is hostile towards the player, usually used in conjunction with the ENEMY tag
								CONTACTABLE,	// Indicates that the instance can be contacted by attacks and other physical sub-routines // This is probably not implemented correctly and will probably be removed, also could be very very slow with large amounts of instances with large lists
								ATTACK,
								PLAYER,			// Some instances look specifically for a nearby player (such as enemy attacks for example)
								ENEMY,			// Usually gives out experience, might get destroyed or rendered differently
								MENU_ELEMENT	// Isolates menu elements (might get rendered on top and destroyed separately) // Maybe remove this tag if it's not really necessary
								}
	
	public List<INSTANCE_TAG> tags; // Make sure to set some tags to your instance, or else it will be invisible
	
	public float previousX = 0; // Instances must manage their own previous positions
	public float previousY = 0; // Makes detection of movement and possible contacts a lot easier
	
	public boolean jumpReady = true; // Saves me a lot of trouble, please and ty
	
	public static final float GRAVITY = 500.0f; // Gravity constant
	
	public boolean direction = true; // Direction the instance is facing, towards right is true (and default).
	public final boolean DIRECTION_LEFT = false;
	public final boolean DIRECTION_RIGHT = true;
		
	public World world; // Current world chunk in which the instance is located.
	
	public Light light; // The instance's light source, if there is one
	
	public Instance(float x, float y, float width, float height, World world) {
		super(x,y,width,height);
		
		this.tags = new ArrayList<INSTANCE_TAG>();
		
		this.world = world;
	}
	
	public void update(int delta) {
				
		previousX = x;
		previousY = y;
		
		// applyGravity(delta);
	}
	
	@Override
	public void render(Graphics g, float offsetx, float offsety) {
		if (renderHitBox) {
			g.setColor(Color.white);
			g.drawRect(x + offsetx, y + offsety, width, height); //default appearance of the instance
			g.drawString("I", x + offsetx + 1, y + offsety + 1);
		}
	}
	
	@Override
	public void render(Graphics g) {
		render(g,0,0);
	}
	
	// React to an a contact with another instance
	public void contact(Instance instance) {
		// Nothing for the default instance
	}
	
	// If the instance is ready to be removed or not
	public boolean isRemovable() {
		return false;
	}
	
	public void inflictDamage(Damage damage) {
		// Nothing, default instances are immune to damage
	}
	
	// This should be moved somewhere else, the instance should not be in charge of handling this
//	/**
//	 * When provided with a set of coordinates, checks whether or not the instance can move to that area.
//	 * 
//	 * @param newX
//	 * 		The potential new x position of the instance.
//	 * @param y
//	 * 		The potential new y position of the instance.
//	 */
//	public boolean collisionCheck(int newX, int newY) { // Executes a collision check from within the instances specified world.
//		
//		Box temporaryPosition = new Box(newX, newY, width, height);
//		
//		for (int checkX = Math.max(firstXPositionInWorld(),0); checkX < Math.min(lastXPositionInWorld(), world.WIDTH-1); x++) {
//			for (int checkY = Math.max(firstYPositionInWorld(),0); checkY < Math.min(lastYPositionInWorld(), world.HEIGHT-1); y++) {
//				
//				Solid element2 = world.solids[checkX][checkY]; // L'instance en cours de traitement
//				
//				if (!element2.hitTest(temporaryPosition)) { // If the initial new postion is bad, adjust it.
//					return false; // Contact with something solid, position in world is not free
//				}
//			}
//		}
//		
//		return true; // Passed all collision tests, position in world is free.
//	}
	
	//TODO Probably move these to the world class, probably better and more modular that way.
	
	/**
	 * Finds the first "tile" that instances touches using its current position.
	 * 
	 * @return The first x postion in the world that the instance touches
	 */
	public int firstXPositionInWorld() {
		
		return (int) Math.floor((x/Parameters.SIZE));
	}
	
	public int firstYPositionInWorld() {
		
		return (int) Math.floor(y/Parameters.SIZE);
	}
	
	public int lastXPositionInWorld() {
		
		return (int) Math.ceil((x + width)/Parameters.SIZE);		
	}
	
	public int lastYPositionInWorld() {
		
		return (int) Math.ceil((y + height)/Parameters.SIZE);
	}
	
	public void applyGravity(int delta) {
		// It seems to be the correct formula
		
		float deltaAdj = delta / 1000.0f; // Adjusted delta (ms to s)
		float vSpeedNew = v_speed + GRAVITY * deltaAdj;
		
		y += (v_speed + vSpeedNew)/2 * deltaAdj;
		v_speed = vSpeedNew;		
		x += h_speed * deltaAdj; // For enemies with simple movement
	}
}
