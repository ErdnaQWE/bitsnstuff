// An instance of the inventory window???

package instances;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import engine.Window;

public class Inventory {
	
	private ArrayList<Item> items = new ArrayList<Item>();
	private int size;	
	
	private int renderTime = 0; // Used for the animation
	private int backgroundAlpha = 0; // The alpha of the background rectangle (that should eventually be replaced by a blur)
	
	//*****RENDER PARAMETERS*****// These should be transfered somewhere else...
	private final int ICONS_PER_ROW = 3; //Number of item icons per row of inventory
	private final int AMOUNT_OF_ALPHA_LINES = 150; // Number of times to run the loop at both ends of the center line to achieve the fading line effect...
	private final int ALPHA_PER_LINE = (int) Math.round(255.0 / AMOUNT_OF_ALPHA_LINES);
	private final int WIDTH_OF_LINE = 4; // Width of the center line
	private final int DISTANCE_OF_EDGE = 150; // Length/Height of the center line, before the blurred segments.
	private final int ALPHA_LENGTH = 150; // Length of the alpha portion at both ends of the center line
	private final int LENGTH_PER_SEGMENT = (int) Math.round(ALPHA_LENGTH / AMOUNT_OF_ALPHA_LINES);
	//***************************
	
	public int getFree() { // Returns the amount of free inventory spots (A negative number indicates the inventory is over-burdened)
		
		return size - items.size();
		
	}
	
	public void addItem(Item item) { // Adds an item to the inventory
		
		items.add(item);
		
	}
	
	public Item takeItem(int index) { // Removes an item from the inventory
		
		return items.remove(index);
		
	}
	
	public Item getItem(int index) { // Returns the item reference without removing from the inventory
		
		return items.get(index);
		
	}
	
	public void setSize(int size) { // Sets the size of the inventory
		
		this.size = size;
		
		// TODO Items should drops if the size of the inventory diminishes...
		
	}
	
	public int getSize() {
		
		return items.size();

	}
	
	public void open() {
		
		// TODO Counting for achievments ???
		renderTime = 0;
		// TODO More initialisaiton of objects ???
		
	}
	
	public void update(int delta) { //Only needs to be updated when opened, some objects will move around...
		
		renderTime += delta;
		
		backgroundAlpha = Integer.min(150, (int) Math.floor(100.0/150 * renderTime));
		
	}

	public void render(Graphics g) {
		
		// Inventory background
		g.setColor(new Color(0,0,0,backgroundAlpha)); //This is where the Blur effect will go when I figure out how to do it :b
		g.fillRect(0,0,Window.getWidth(), Window.getHeight());
		
		// Middle Line
		g.setColor(Color.white); // TODO Make the line fade in correctly
		g.fillRect(Window.getWidthMiddle() - WIDTH_OF_LINE / 2 , DISTANCE_OF_EDGE, WIDTH_OF_LINE, Window.getHeightMiddle() - DISTANCE_OF_EDGE * 2); //BASE LINE
		
		for (int i = 0; i < AMOUNT_OF_ALPHA_LINES; i++) {
			
			g.setColor(new Color(255,255,255, 255 - ALPHA_PER_LINE * i));
			g.fillRect(Window.getWidthMiddle() - WIDTH_OF_LINE / 2, DISTANCE_OF_EDGE - LENGTH_PER_SEGMENT * i, WIDTH_OF_LINE, LENGTH_PER_SEGMENT);
			g.fillRect(Window.getWidthMiddle() - WIDTH_OF_LINE / 2, Window.getHeight() - DISTANCE_OF_EDGE + LENGTH_PER_SEGMENT * i, WIDTH_OF_LINE, LENGTH_PER_SEGMENT);

		}
		
		// Items
		int h_pos = 0;
		int v_pos = 0;
		
		for (int i = 0; i < items.size(); i ++) {
			
			items.get(i).render(g, Window.getWidthMiddle() + 16 + 80 * h_pos , 16 + 80 * v_pos);
			
			h_pos += 1;
			
			if (h_pos > ICONS_PER_ROW) {
				v_pos += 1;
				h_pos = 0;
			}
			
		}
		
	}
	
}
