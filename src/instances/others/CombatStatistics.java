package instances.others;

import java.util.ArrayList;
import java.util.List;

public class CombatStatistics {

	public int health;
	public int secondary; // A secondary attribute like mana
	
	public int experiencePoints; // The base amount of experience points that enemies will give to the player
	
	public enum DROP_CLASS {NORMAL, BOSS, NONE} // The type of loot that the instance could drop, basically decides of the loot table that will be used
	public List<DROP_CLASS> dropClasses;
	public int dropRate; // The drop rate of the enemy
	
	public CombatStatistics() {
		this.health = 0;
		this.secondary = 0;
		
		this.experiencePoints = 0;
		
		this.dropClasses = new ArrayList<DROP_CLASS>();
		this.dropRate = 0;
	}
}
