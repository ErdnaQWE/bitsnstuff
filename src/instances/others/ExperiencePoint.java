// TODO At some point, make sure that the experience points are attracted to the nearest player and not just the first one that it finds in the Hitbox list.

package instances.others;

import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import instances.Hitbox;
import instances.Instance;
import world.World;

public class ExperiencePoint extends Instance {

	private Instance currentTargetPlayer;
	private Hitbox hitbox;
	
	private float currentXAccel;
	private float currentYAccel;
	
	private boolean consummed = false;
	
	private static final float totalAcceleration = 1000; // In pixels per second
	
	public ExperiencePoint(float x, float y, int expValue, World world) {
		super(x, y, expValue * 2, expValue * 2, world);
				
		LinkedList<INSTANCE_TAG> tagList = new LinkedList<INSTANCE_TAG>();
		tagList.add(INSTANCE_TAG.PLAYER);
		this.hitbox = new Hitbox(getCenterX(), getCenterY(), 400, 400, world, tagList, false);
		world.instances.add(this.hitbox);
		
		currentXAccel = 0;
		currentYAccel = 0;
	}
	
	@Override
	public void update(int delta) {
		// Find the current nearest player within a certain range, probably with a custom "hitbox". (Doesn't actually compute the nearest one, just picks the first one for now)
		List<Instance> foundPlayers = hitbox.getFoundInstances();
		if (!foundPlayers.isEmpty()) {
			currentTargetPlayer = foundPlayers.get(0);
		} else {
			currentTargetPlayer = null;
		}
		
		// Accelerate or slow down the point
		if (currentTargetPlayer != null) {
			// Calculate acceleration fractions
			float xDistance = currentTargetPlayer.getCenterX() - this.getCenterX();
			float yDistance = currentTargetPlayer.getCenterY() - this.getCenterY();
			
			float totalDistance = Math.abs(xDistance) + Math.abs(yDistance);
			
			float xFraction = xDistance/totalDistance;
			float yFraction = yDistance/totalDistance;
			
			currentXAccel = totalAcceleration * xFraction;
			currentYAccel = totalAcceleration * yFraction;
			
			float deltaAdj = delta / 1000.0f; // Adjusted delta (ms to s)
			float hSpeedNew = h_speed + currentXAccel * deltaAdj;
			float vSpeedNew = v_speed + currentYAccel * deltaAdj;
			
			x += (h_speed + hSpeedNew)/2 * deltaAdj;
			h_speed = hSpeedNew;

			y += (v_speed + vSpeedNew)/2 * deltaAdj;
			v_speed = vSpeedNew;		
			
		} else {
			float deltaAdj = delta / 1000.0f; // Adjusted delta (ms to s)
			float hSpeedNew = h_speed * (1 - (0.95f * deltaAdj));
			float vSpeedNew = v_speed * (1 - (0.95f * deltaAdj));
			
			x += (h_speed + hSpeedNew)/2 * deltaAdj;
			h_speed = hSpeedNew;

			y += (v_speed + vSpeedNew)/2 * deltaAdj;
			v_speed = vSpeedNew;	
		}
		
		// Update the position of the hitbox for the next frame
		hitbox.x = getCenterX() - (hitbox.width / 2f);
		hitbox.y = getCenterY() - (hitbox.height / 2f);
		
		
		// Note to self for next time I code:
			// Maybe make it so it can catch any instance with or without certain tags (YES! Perfect!)
			// Also maybe rework how the tiles are rendred so this is possible with the environment too!
		
		// Also create a cool animation for when bit levels up
			// Have some lines go in random directions, the higher the level the longer the lines?
			// Circles at the end of the lines, something technical
		
		// Accelerate the exp in the target player's direction
			// When the player touches an experience point, make him "flash and grow"
	}
	
	@Override
	public void render (Graphics g, float offsetX, float offsetY) {
		g.setColor(Color.white);
		g.fillRect(x + offsetX, y + offsetY, width, height);
		
		if (width >= 4) {
			g.setColor(Color.black);
			g.drawRect(x + offsetX, y + offsetY, width, height);
		}
	}
	
	@Override
	public void contact(Instance otherInstance) {
		if (otherInstance.tags.contains(INSTANCE_TAG.PLAYER)) {
			consummed = true;
			this.hitbox.consume();
		}
	}
	
	@Override
	public boolean isRemovable() {
		return consummed;
	}
}
