package instances;

import java.util.Arrays;

import world.World;

public class Attack extends Instance {
			
	protected int time; // Multiple attacks will operate based on time elapsed
	
	public Damage carryingDamage; // The damage that will be applied to the contacted enemy
	
	public Attack (float x, float y, float width, float height, boolean hostile, World world) {
		
		super(x, y, width, height, world);
		
		this.tags = Arrays.asList(
			INSTANCE_TAG.DAMAGEABLE,
			INSTANCE_TAG.CONTACTABLE,
			INSTANCE_TAG.ATTACK
		);
		
		this.time = 0;
	}
}
