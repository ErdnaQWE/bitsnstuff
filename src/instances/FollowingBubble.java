// A text bubble that should follow the specified instance

package instances;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;

import txtBox.DialogueText;
import world.World;

public class FollowingBubble extends Instance {
	
	private Instance followingInstance; // The instance that the speech bubble will follow
	private DialogueText currentText;

	public FollowingBubble(float x, float y, float width, float height, World world, Instance followingInstance) {
		super(x, y, width, height, world);
		
		this.followingInstance = followingInstance;
		String[] currentText = {"%NORMAL% Manger un canard mais",
								"%NORMAL% on ne mange pas des enfants."};
		this.currentText = new  DialogueText(currentText, x, y);
	}
	
	@Override
	public void update(int delta) {
		currentText.update(delta);
	}
	
	@Override
	public void render (Graphics g, float offsetX, float offsetY) {
		
		float[] points = {
				x + (width/2) - 40,
				y + (height/2),
				x + (width/2) + 40,
				y + (height/2),
				followingInstance.x + offsetX - 5,
				followingInstance.y + offsetY - 5
		};
		Shape triangle = new Polygon(points);
		
		g.setColor(Color.gray);
		g.fillOval(x + 8, y + 8, width, height);
		
		g.setColor(Color.white);
		g.fill(triangle);
		
		g.setColor(Color.white);
		g.fillOval(x, y, width, height);
		
		g.setColor(Color.black);
		currentText.render(g, x - (width/4), y - (height/4));
	}
}
