package instances.enemies;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SpriteSheet;

import engine.Resources;
import instances.Instance;
import instances.others.CombatStatistics;
import world.World;

public class Mole extends Instance {
		
	private boolean submerged = true; // Moles are initially underground, by default
	private int lastUpdate = 0;
	
	private int currentAnimationFrame;
	private int timeUntilNextFrame;
	private static final int TIME_PER_FRAME = 80;
	
	private CombatStatistics currentStats;
	
	private int spriteXOffset = -36;
	private int spriteYOffset = -42;
	
	private SpriteSheet currentSprite = Resources.getSprite("MoleWalk");

	public Mole(float x, float y, World world) {
		this (x, y, world, false);
	}
	
	public Mole(float x, float y, World world, boolean submerged) {
		super(x, y, 56, 56, world);
		this.submerged = submerged;
		
		this.currentStats = new CombatStatistics();
		currentStats.health = 10;
		
		currentAnimationFrame = 0;
		timeUntilNextFrame = TIME_PER_FRAME;
		
		this.tags.add(INSTANCE_TAG.ENEMY);
		this.tags.add(INSTANCE_TAG.CONTACTABLE);
		this.tags.add(INSTANCE_TAG.HOSTILE);
		this.tags.add(INSTANCE_TAG.TANGIBLE);
	}
	
	private void animationUpdate(int delta) {
		if (h_speed != 0) {
			timeUntilNextFrame -= delta;
		} else {
			timeUntilNextFrame = TIME_PER_FRAME;
			currentAnimationFrame = 0;
		}
		
		if (timeUntilNextFrame <= 0) {
			timeUntilNextFrame += TIME_PER_FRAME;
			currentAnimationFrame += 1;
			
			if (currentAnimationFrame == currentSprite.getHorizontalCount()) {
				currentAnimationFrame = 1; // Skip the first frame
			}
		}
	}
	
	@Override
	public void update(int delta) {
		super.update(delta);
		
		applyGravity(delta);
		
		lastUpdate += delta;
		
		if (lastUpdate > 999) {
			lastUpdate -= 1000;
			h_speed = 0;
			
			if (!submerged) {
				int randChoice = (int) Math.floor(Math.random() * 3);
				
				switch (randChoice) {
					case 0: 
						submerged = true;
						//TODO Trigger animation... somehow...
						break;
						
					case 1:
						//TODO Throw a rock
						// then move (hence no break)
					
					case 2:
						//Move
						// int randomMove = (int) Math.floor(Math.random() * 3); //Mole will move intelligently only once every three moves
						
						// This has to be redone since players are no longer singled out
						
						/*if ((world.players.get(0).getCenterX() > x && randomMove == 0) || (world.players.get(0).getCenterX() < x && randomMove != 0)) { // then move to the left
							h_speed = -40;
							direction = true;
						} else {
							h_speed = 40;
							direction = false;
						}*/
						break;
					}
						
			}
		} else {
			int randChoice = (int) Math.floor(Math.random() * 3);
			
			switch (randChoice) {
				case 0:
					submerged = false;
			}
		}
		
		animationUpdate(delta);
	}
	
	@Override
	public void render (Graphics g, float offsetx, float offsety) {
		g.drawImage(currentSprite.getSubImage(currentAnimationFrame,0).getScaledCopy(2).getFlippedCopy(direction, false), x + offsetx + spriteXOffset, y + offsety + spriteYOffset);
	}
	
	@Override
	public void contact(Instance instance) {
		if (instance.tags.contains(INSTANCE_TAG.PLAYER) && instance.previousY + instance.height <= y) { // The player can jump on the mole, kind of a debug thing
				instance.v_speed = -300;
				currentStats.health -= 10 * 0.25; // DEBUG, jumping on the enemy, remove
		}
	}
	
	@Override
	public boolean isRemovable() {
		return currentStats.health < 0;
	}
}
