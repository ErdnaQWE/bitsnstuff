package instances;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;

import world.World;

public class Hitbox extends Instance {

	// A Hitbox that "collects" the instances that it touches until it is marked as consumed.
	
	private LinkedList<INSTANCE_TAG> targetTags;
	private boolean findAll; // Whether or not to find only one or all of the tags before approving the instances for contact
	private boolean consumed; // Whether or not the list of caught instances has been consumed
	private List<Instance> foundInstances;
	private List<Instance> previouslyFoundInstances;
	
	public Hitbox(float x, float y, float width, float height, World world, List<INSTANCE_TAG> targetTags, boolean findAll) {
		super(x, y, width, height, world);
		
		this.findAll = findAll;
		this.consumed = false;
		
		this.targetTags = new LinkedList<INSTANCE_TAG>();
		this.foundInstances = new LinkedList<Instance>();
		this.previouslyFoundInstances = foundInstances;
		Iterator<INSTANCE_TAG> iter = targetTags.iterator();
		while (iter.hasNext()) this.targetTags.add(iter.next());
	}

	@Override
	public void update(int delta) {
		previouslyFoundInstances = foundInstances;
		foundInstances = new LinkedList<Instance>();
	}
	
	@Override
	public void contact(Instance contactedInstance) {
		if (!consumed) {
			Iterator<INSTANCE_TAG> iter = targetTags.iterator();
			while(iter.hasNext()) {
				if (contactedInstance.tags.contains(iter.next())) {
					if (!findAll) {
						foundInstances.add(contactedInstance);
						break;
					}
				} else {
					if (findAll) return;
				}
			}
			
			if (findAll) foundInstances.add(contactedInstance);
		}
	}
	
	@Override
	public boolean isRemovable() {
		return consumed;
	}
	
	/**
	 * Mark the hitbox as being consumed, this will destroy the instance.
	 * Sometimes the hitbox will be immediately marked as consumed (since the instance using it only needs to search for one frame).
	 */
	public void consume() {
		this.consumed = true;
	}
	
	/**
	 * Get the most recent list of found instances.
	 * Note that it is the instances' responsibility to check whether this list was already given or not (since there might be some irregularities in the timing)
	 * TODO: Make it this modules responsibility to check for this?
	 * @return The most recent list of found instances.
	 */
	public List<Instance> getFoundInstances() {
		// In some situations, the list could have gotten cleared before it was used by an instance.
		// Using a buffer in this fashion assures that always at lease one valid list is available.
		if (foundInstances.isEmpty()) {
			return previouslyFoundInstances;
		} else {
			return foundInstances;
		}
	}
}
