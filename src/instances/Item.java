package instances;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class Item {
	
	public String name; // Name of the item
	public String description; // Flavour text, displayed at the bottom
	
	public int level; // The item's level
	public int levelRequirment; // The required level to use the item
	public int value; // The item's gold value (-1 for items that cannot be sold)
	
	public Image icon; // The item's image
	public Image infoPanel; // The fully rendered info panel of the item
	
	private int lastRenderedX = -1000;
	private int lastRenderedY = -1000;
	
	public Item() throws SlickException { // TODO Also watch out for this exception!!!
		
		// Default item infos
		
		this.name = "Null & Void";
		this.description = "The developper screwed up somehow...";
		
		this.level = 1;
		this.levelRequirment = 0;
		this.value = 1;
		
		renderInfo(); // TODO Place at a more strategic place...
		
	}
	
	public void render(Graphics g, int x, int y) {
		
		//Render the inventory icon for the item
		if (icon == null) {
			
			g.setColor(Color.black);
			g.fillRect(x,y,64,64);
			
		} else {
			g.drawImage(icon, x, y);
		}
		
		lastRenderedX = x;
		lastRenderedY = y;
		
	}
	
	public void renderInfo() throws SlickException { // TODO WATCH OUT FOR THIS EXCEPTION!!!
		
		Image infoPanel = new Image(200, 300);
		Graphics infoPanelG = infoPanel.getGraphics(); // WARNING!!! This operation might take a long time apparently
		
		infoPanelG.setColor(Color.orange);
		infoPanelG.fillRect(0,0,infoPanel.getWidth(), infoPanel.getHeight());
		
		render(infoPanelG, 3, 3); // Item icon
		
		infoPanelG.setColor(Color.white);
		infoPanelG.drawString(name, 67, 3);
		
		infoPanelG.flush();
		
		this.infoPanel = infoPanel;	
		
	}
	
	public boolean checkHover(int x, int y) {
		
		// A collision check to verify that the specified point is over the item's last rendered coordinate.
		if (x >= lastRenderedX && x <= lastRenderedX + 64 && y >= lastRenderedY && y <= lastRenderedY  + 64) {
			return true;
		} else {
			return false;
		}
		
	}

}
