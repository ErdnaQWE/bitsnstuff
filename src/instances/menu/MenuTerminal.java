package instances.menu;

import java.util.Iterator;
import java.util.LinkedList;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input; // Find a way to avoid this somehow
import org.newdawn.slick.KeyListener; // Especially this too hehe

import engine.CurrentInput;
import engine.Window;
import instances.Instance;

public class MenuTerminal extends Instance implements KeyListener {

	private long msAlive; // Used for animations, probably a good idea to rename
	
	private static final int WINDOW_WIDTH = 800;
	private static final int RENDER_X = Window.getWidthMiddle() - (WINDOW_WIDTH / 2);
	private static final int RENDER_Y = Window.getHeight();
	private static final int TEXT_ROW_HEIGHT = 18; // Height of a single line of text in the console, should match the font height
	private static final int CONSOLE_SIDE_PADDING = 8; // Padding on the side of the console before the text starts to get rendered
	private static final int CONSOLE_TOP_PADDING = 4; // Padding on the top of the console before the text starts to get rendered
	private static final int CONSOLE_BOTTOM_PADDING = 8;
	
	private float currentHeight;
	private int destinationHeight;
	private LinkedList<String> textList;
	
	private Input retainedInputObject; // In order to remove the listener once done?
	private LinkedList<String> retainedInput;
	private String currentUserInput = "> ";
	
	public MenuTerminal(CurrentInput currentInput) {
		
		super(0, 0, 0, 0, null);
		// Chose not to have a custom position for the console, it will always spawn at the same place on the screen
		// Also world shouldn't matter here
		
		this.tags.add(INSTANCE_TAG.MENU_ELEMENT);
		
		this.msAlive = 0;
		this.currentHeight = 1;
		this.destinationHeight = 1;
		
		this.textList = new LinkedList<String>();
		this.textList.add("B & S Console v0.0");
		
		this.retainedInputObject = currentInput.getInputObject();
		retainedInputObject.addKeyListener(this);
		this.retainedInput = new LinkedList<String>();
	}
	
	@Override
	public void update(int delta) {
		msAlive += 0;
		
		while(!retainedInput.isEmpty()) {
			textList.add(retainedInput.pop());
		}
		
		destinationHeight = ((textList.size() + 1) * (TEXT_ROW_HEIGHT)) + CONSOLE_TOP_PADDING + CONSOLE_BOTTOM_PADDING;
		currentHeight = destinationHeight;
	}
	
	@Override
	public void render(Graphics g, float offsetX, float offsetY) {
		// Render the box
		g.setColor(Color.white);
		g.fillRect(RENDER_X, RENDER_Y, WINDOW_WIDTH, -currentHeight);
		
		// Render the text
		g.setColor(Color.black);
		Iterator<String> iterator = textList.iterator();
		int currentTextHeight = 0;
		while(iterator.hasNext()) {
			g.drawString(iterator.next(), RENDER_X + CONSOLE_SIDE_PADDING, RENDER_Y - currentHeight + currentTextHeight + CONSOLE_TOP_PADDING);
			currentTextHeight += TEXT_ROW_HEIGHT;
		}
		g.drawString(currentUserInput, RENDER_X + CONSOLE_SIDE_PADDING, RENDER_Y - currentHeight + currentTextHeight + CONSOLE_TOP_PADDING);
	}
	
	@Override
	public boolean isRemovable() {
		if (currentHeight <= 0) {
			retainedInputObject.removeKeyListener(this);
			return true;
		} else {
			return false;
		}
	}

	public void keyPressed(int key, char c) {
		if (key == 28) {
			retainedInput.add(currentUserInput);
			currentUserInput = "> ";
		} else if (key == 14) {
			if (currentUserInput.length() > 2) {
				currentUserInput = currentUserInput.substring(0, currentUserInput.length() - 1);
			}
		} else if (c != 0) {
			currentUserInput = currentUserInput + c;
			System.out.println(key);
		}
	}
	
	public void keyReleased(int key, char c) {
		// Do nothing, it doens't matter
	}

	@Override
	public void inputEnded() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputStarted() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isAcceptingInput() {
		return true;
	}

	@Override
	public void setInput(Input arg0) {
		// TODO Auto-generated method stub
		
	}
}
