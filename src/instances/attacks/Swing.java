package instances.attacks;

import org.newdawn.slick.Graphics;

import instances.Attack;
import instances.Damage;
import instances.Instance;
import world.World;

public class Swing extends Attack {
	
	public boolean enabled = false; // Simplifies things for animations, thank you
	
	private final float LIFETIME = 400;
	
	private boolean readyForErasure = false;
	
	public Swing(float x, float y, float width, float height, float damage, World world) {
		super(x, y, width, height, false, world);
		
		this.carryingDamage = new Damage(damage);
	}

	@Override
	public void update (int delta) {
		
		super.update(delta);
		
		//TODO Has to follow a certain path, somehow
		if (!enabled && time > 185) { // estimate
			enabled = true;
		}
		
		if (enabled) {
			y += delta / 1000.0 * 250.0;
			
			if (time > LIFETIME) {
				readyForErasure = true;
			}
		}		
	}
	
	@Override
	public void render(Graphics g, float offsetx, float offsety) {
		if (enabled) {
			super.render(g, offsetx, offsety);
		}	
	}
	
	@Override
	public void contact(Instance instance) {
		if (tags.contains(INSTANCE_TAG.HOSTILE) != instance.tags.contains(INSTANCE_TAG.HOSTILE) && instance.tags.contains(INSTANCE_TAG.DAMAGEABLE)) { // No friendly fire hehe
			instance.inflictDamage(carryingDamage);
		}
	}
	
	@Override
	public boolean isRemovable() {
		return readyForErasure;
	}
}
